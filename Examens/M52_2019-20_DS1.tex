\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M52}
\newcommand*{\app}[5]
{#1: \left\{
\begin {array}{ccl}
  #2 & \longrightarrow & #3 \\
  \displaystyle #4 & \longmapsto & \displaystyle #5
\end {array}\right.}

% pour voir les solutions il faut enlever le commentaire de la ligne suivante
% \solutionstrue

\begin{document}

% ==================================
\hautdepage{
\sisujet{DS1}\sisolutions{Solutions du DS2}\\
  \normalfont\normalsize
 13 novembre 2019\\
  {[ durée: 2 heures ]}
}
% ==================================
\sisujet{
  \attention~\emph{Les documents ne sont pas autorisés. Le sujet comporte trois exercices qui pourront être traités dans l'ordre de votre choix.}
  \bigskip
  \tsvp
}
% ==================================


% -----------------------------------
\begin{exo} (Questions de cours) \emph{Les trois questions de cours sont indépendantes.}

  Soit $(X,d)$ un espace métrique.
  \begin{enumerate}
    \item Montrer que pour tous $(x,y,z)\in X^3$, on a
    \[
      d(y,z)\geq |d(x,y)-d(x,z)|.
    \]
    \item Soit $x\in X$ et $r>0$. Montrer que $B(x,r)=\ensemble{y}{d(x,y)<r}$ est un ensemble ouvert de $X$.
    \item Soit $f:X\longrightarrow X$ une application et $x\in X$. Montrer que $f$ est continue en $x$ si et seulement si pour toute suite $\suiteN{x_n}\in X^{\mathbb{N}}$ qui converge vers $x$, alors la suite $\suiteN[size=\big]{f(x_n)}$ converge vers $f(x)$.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Soient $x,y,z \in X$. L'inégalité triangulaire assure $d(x,y) \leq d(x,z)+d(z,y)$, d'où $d(y,z)=d(z,y) \geq d(x,y) - d(x,z)$. En échangeant les rôles de $y$ et $z$, on obtient $d(y,z) \geq d(x,z) - d(x,y)$ (on peut aussi écrire l'inégalité triangulaire $d(x,z) \leq d(x,y)+d(y,z)$). Les deux inégalités précédentes signifient $d(y,z) \geq \abs{d(x,y)-d(x,z)}$.
    \item Soit $y \in B(x,r)$. Alors $r-d(x,y)>0$ et la boule ouverte $B(y,r-d(x,y))$ est contenue dans $B(x,r)$. En effet, si $z \in B(y,r-d(x,y))$, alors $d(x,z) < d(x,y) + d(y,z) < d(x,y) + (r-d(x,y)) = r$. Cela signifie exactement que $B(x,r)$ est ouvert.
    \item On dit que $f$ est continue en $x$ si $\forall \varepsilon > 0, \exists \delta > 0 \;|\; \forall x' \in X, d(x',x) < \delta \Rightarrow d(f(x'),f(x)) < \varepsilon$.\\
    Supposons $f$ continue en $x$. Soit $\suiteN{x_n}$ une suite de limite $x$. Montrons que $\suiteN{f(x_n)}$ tend vers $f(x)$. Soit donc $\varepsilon >0$. Par continuité de $f$ en $x$, il existe $\delta >0$ tel que $d(x',x)<\delta \Rightarrow d(f(x'),f(x)) < \varepsilon$. Puisque $(x_n)$ tend vers $x$, il existe $N_0$ tel que $n \geq N_0 \Rightarrow d(x_n,x) < \delta$. Soit $n \geq N_0$, Alors $d(x_n,x) < \delta$, d'où $d(f(x_n),f(x)) < \varepsilon$. La suite $\suiteN{f(x_n)}$ converge donc vers $f(x)$.\\
    Supposons maintenant $f$ non continue en $x$. Alors il existe $\varepsilon > 0$ tel que $\forall \delta > 0, \exists x', d(x,x') < \delta \text{ et } d(f(x),f(x')) \geq \varepsilon$. Fixons un tel $\varepsilon > 0$. Alors, pour tout $n \geq 1$, il existe $x_n$ tel que $d(x_n,x) < 1/n$ et $d(f(x_n),f(x)) > \varepsilon$. La suite $\suite[index={n \geq 1}]{x_n}$ ainsi construite tend vers $x$, mais $f(x_n)$ ne tend pas vers $f(x)$ (car, pour tout $n \geq 1$, $f(x_n) \notin B(f(x),\varepsilon)$).\\
    \emph{Conclusion:} $f$ est continue en $x$ si et seulement si pour toute suite $\suiteN{x_n}$ convergeant vers $x$, la suite $\suiteN{f(x_n)}$ tend vers $f(x)$.
  \end{enumerate}
\end{solution}


% -----------------------------------
\begin{exo} (Linéarité dans l'espace des suites)

  Soit
  \[
    c_0=\ensemble[\big]{\suiteN{u_n}\in\mathbb{R}^{\mathbb{N}}}{\lim_{n\to+\infty}u_n=0}
  \]
  et
  \[
    \ell^{\infty}=\ensemble[\big]{\suiteN{u_n}\in\mathbb{R}^{\mathbb{N}}}{\suiteN{u_n} \text{ est bornée}}.
  \]
  On rappelle que le $\mathbb{R}$-espace vectoriel $\ell^\infty$ est muni de la norme
  \[
    \norm{u}_\infty:=\sup_{n\geq 0}|u_n|,\quad\text{pour } u=\suiteN{u_n}\in\ell^\infty.
  \]
  \begin{enumerate}
    \item Justifier que $c_0$ est un sous-espace vectoriel de $\ell^\infty$.
    \item Montrer que $c_0$ est fermé dans $\ell^\infty$.
    \item Montrer que l'intérieur de $c_0$ est vide.
    \item Soit
    \[
      \app{\varphi}{c_0}{\mathbb{R}}{\suiteN{u_n}}{\sum_{n=1}^{+\infty}\frac{u_n}{2^n}}.
    \]
    \begin{enumerate}
      \item Vérifier que $\varphi$ est bien définie, linéaire et continue, et montrer que $\norm{\varphi}_{\mathcal{L}(c_0,\mathbb{R})}\leq 1$.
      \item Montrer que $\norm{\varphi}_{\mathcal{L}(c_0,\mathbb{R})}=1$.
    \end{enumerate}
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Rappelons qu'une suite convergente est bornée: en effet, si $(u_n)$ tend vers $l$, alors il existe $N$ tel que $n \geq N \Rightarrow |u_n-l| < 1$, et $\suiteN{\abs{u_n}}$ est donc majorée par $\max (1+|l|,\abs{u_0},\abs{u_1},\ldots,\abs{u_{N-1}})$. Toute suite de limite nulle appartient donc à $\ell^\infty$: autrement dit, $c_0 \subset \ell^\infty$.\\
    De plus, la suite nulle appartient à $c_0$, et toute combinaison linéaire de suites de limite nulle est une suite de limite nulle, ce qui fait de $c_0$ un sous-espace vectoriel de $\ell^\infty$.
    \item Soit $\suite[index=k \in \mathbb{N}]{u^{(k)}} \in c_0^\mathbb N$ une suite à valeurs dans $c_0$ admettant une limite $u$ dans $\ell^\infty$. Soit $\varepsilon>0$. Par hypothèse, il existe $k_0$ tel que $\norm{u-u^{(k_0)}}_\infty < \varepsilon/2$. Comme $u^{(k_0)} \in c_0$, il existe $N$ tel que $n \geq N \Rightarrow |u^{(k_0)}_n|<\varepsilon/2$. On a alors $n \geq N \Rightarrow |u_n| \leq |u_n - u^{(k_0)}_n| + |u^{(k_0)}_n| \leq \norm{u-u^{(k_0)}}_\infty + |u^{(k_0)}_n| < \varepsilon/2 + \varepsilon/2=\varepsilon$. (On peut préférer montrer que le complémentaire est ouvert: soit $u \notin c_0$. Cela signifie qu'il existe $\varepsilon>0$ tel que $\forall N, \exists n \geq N, |u_N|> \varepsilon$. On remarque alors que $B(u,\varepsilon/2)$ ne rencontre pas $c_0$: en effet, si $v$ vérifie $\norm{u-v}_\infty<\varepsilon/2$, alors pour tout $N$ il existe $n \geq N$ tel que $|v_n| > \varepsilon/2$.)
    \item On peut rappeler qu'un sous-espace vectoriel strict est toujours d'intérieur vide. On peut aussi directement écrire pour $u \in c_0$: pour tout $r>0$, la suite $\suiteN{u_n + r/2}$ appartient à $B(u,r)$ mais pas à $c_0$ (elle tend vers $r/2  \neq 0$), de sorte que $u$ n'est pas intérieur à $c_0$.
    \item
    \begin{enumerate}
      \item Soit $u \in c_0$. La suite $u$ est bornée, par $\norm{u}_\infty$. Comme la série $\sum_n \frac{\norm{u}_\infty}{2^n}$ converge, la série $\sum \frac{u_n}{2^n}$ est absolument convergente (dans $\mathbb R$), donc convergente (dans $\mathbb R$). L'application $\varphi$ est donc bien définie. La linéarité est claire. Enfin, si $u \in c_0$, alors pour tout $N$, $|\sum_{n=1}^N \frac{u_n}{2^n}|\leq \sum_{n=1}^N \frac{\norm{u}_\infty}{2^n} \leq \norm{u}_\infty \sum_{n=1}^{+\infty} \frac{1}{2^n} = \norm{u}_\infty$, ce qui entra\^ine $|\varphi(u)| \leq \norm{u}_\infty$. L'application linéaire $\varphi$ est donc $1$-lipschitzienne, donc continue, et sa norme est inférieure ou égale à $1$: $\norm{\varphi}_{\mathcal{L}(c_0,\mathbb{R})} \leq 1$.
      \item Considérons pour tout $N$ la suite stationnaire $u^{(N)}$ définie par $u^{(N)}_n=1$ si $n \leq N$, $0$ sinon. Alors, pour tout $N$, $u^{(N)} \in c_0$, $\norm{u^{(N)}}_\infty=1$ et $|\varphi(u^{(N)})|=\sum_{n=1}^N \frac{1}{2^n}=1-\frac{1}{2^{N}}$. On a donc, pour tout $N$, $\norm{\varphi}_{\mathcal{L}(c_0,\mathbb{R})}\geq 1-\frac{1}{2^{N}}$. Donc $\norm{\varphi}_{\mathcal{L}(c_0,\mathbb{R})}\geq 1$, puis, finalement, $\norm{\varphi}_{\mathcal{L}(c_0,\mathbb{R})}= 1$.
    \end{enumerate}
  \end{enumerate}
\end{solution}

% -----------------------------------
\begin{exo} (Distance \textsc{sncf})

  Pour un nombre complexe $z$, on note $\arg(z)$ son argument\footnote{En fixant arbitrairement que $\arg(0)=0$.} dans l'intervalle $[0,2\pi[$. Pour $(z_1,z_2)\in\mathbb{C}^2$ on définit
  \[
    d_s(z_1,z_2) =
    \begin{cases}
      |z_1-z_2|   & \text{si } \arg(z_1)=\arg(z_2), \\
      |z_1|+|z_2| & \text{sinon.}
    \end{cases}
  \]
  \begin{center}
    \begin{minipage}{35mm}
      \centering
      \includegraphics[width=35mm]{M52_2019-20_DS1_img_exo3a.pdf}\\[-.21\baselineskip]
      $\arg(z_1)=\arg(z_2)$
    \end{minipage}
    \qquad
    \begin{minipage}{35mm}
      \centering
      \includegraphics[width=35mm]{M52_2019-20_DS1_img_exo3b.pdf}\\[-.21\baselineskip]
      $\arg(z_1)\neq\arg(z_2)$
    \end{minipage}
  \end{center}
  \bigskip
  \begin{enumerate}
    \item Vérifier que $d_s$ est une distance sur $\mathbb{C}$. \emph{(On appelle cette distance la distance \textsc{sncf}.)}
    \item Soit $z_0=2$.
    \begin{enumerate}
      \item Déterminer $B_{d_s}(z_0,1)$, la boule ouverte de centre $z_0$ et de rayon $1$ pour la distance $d_s$.
      \item Soit $d_u$ la distance usuelle sur $\mathbb{C}$, c'est-à-dire $d_u(z_1,z_2)=|z_1-z_2|$ pour $(z_1,z_2)\in\mathbb{C}^2$.\\ Est-ce que $B_{d_s}(z_0,1)$ est un ouvert pour $(\mathbb{C},d_u)$? Justifier.
      \item Est-ce que les distances $d_u$ et $d_s$ sont équivalentes ?
    \end{enumerate}
    \item On notera $X_u$ l'ensemble $\mathbb{C}$ muni de la distance $d_u$, et $X_s$ l'ensemble $\mathbb{C}$ muni de la distance $d_s$.
    \begin{enumerate}
      \item Montrer que l'application $\app{f}{X_s}{X_u}{z}{z+1} $ est lipschitzienne.
      \item Montrer que l'application $\app{f}{X_s}{X_s}{z}{z+1} $ n'est pas continue.\\[7pt]
      \begin{indication}
        on pourra considérer la suite $z_n=\frac{i}{n}$, $n\geq 1$.
      \end{indication}
    \end{enumerate}
  \end{enumerate}
\end{exo}


\begin{solution}
  \begin{enumerate}
    \item Vérifions que $d_s$ est une distance. C'est une application à valeurs positives. Remarquons que pour tous $x,y \in \mathbb C$, $|x-y| \leq d_s(x,y) \leq |x|+|y|$.\\
    Si $x=y$, alors $\arg x= \arg y$ et $d_s(x,y)=0$. Réciproquement, si $d_s(x,y)=0$, alors $|x-y|\leq d_s(x,y)=0$, d'où $x=y$. L'axiome de séparation est vérifié.\\
    Soient $x,y \in \mathbb C$. S'ils ont même argument, alors $d_s(x,y)=|x-y|=|y-x|=d_s(y,x)$, et, sinon, $d_s(x,y)=|x|+|y|=|y|+|x|=d_s(y,x)$. Ainsi $d_s$ est symétrique.\\
    Soient $x,y,z \in \mathbb C$. Si $\arg x = \arg y$ alors $d_s(x,y)=|x-y| \leq |x-z|+|z-y| \leq d_s(x,z)+d_s(z,y)$. Sinon, $\arg x \neq \arg y$, et $\arg z \neq \arg x$ ou $\arg z \neq \arg y$. Dans le premier cas, $d_s(x,y)=|x|+|y|=|x|+|z+y-z| \leq |x|+|z|+|y-z| = d_s(x,z) + |y-z| \leq d_s(x,z)+d_s(z,y)$. Le second cas se traite de la même manière (ou en utilisant la symétrie). L'inégalité triangulaire est donc vérifiée.
    \item
    \begin{enumerate}
      \item Remarquons que $\arg z \neq 0=\arg 2$ entra\^ine $d_s(2,z) =|2|+|z| \geq 2$. Ainsi, si $z \in B_{d_s}(2,1)$, alors $\arg z=0$ (i.e. $z \in \mathbb R_+$) et $|2-z|=d_s(2,z) < 1$ entra\^ine $z \in ]1,3[$. L'inclusion réciproque est immédiate. Ainsi $B_{d_s}(2,1)=]1,3[$.
      \item L'ensemble $B_{d_s}(z_0,1)=]1,3[$ n'est pas ouvert pour $d_u$. En effet, il ne contient aucune boule ouverte (pour $d_u$) centrée en $z_0=2$: pour tout $r>0$, $B_{d_u}(2,r)$ contient le point $2+\frac{r}{2} i$ qui n'appartient pas à $]1,3[$.
      \item Les distances $d_u$ et $d_s$ ne sont pas équivalentes: en effet, deux distances équivalentes sont topologiquement équivalentes et ont donc les mêmes ouverts, or on vient de voir que l'ensemble $]1,3[$ est ouvert pour $d_s$ mais pas pour $d_u$. (On peut sinon remarquer qu'il n'existe aucun $C$ tel que $d_s(x,y) \leq C d_u(x,y)$ pour tous $x,y \in \mathbb C$, car on aurait par exemple $n \leq d_s(n,n+i) \leq C d_u(n,n+i)= C$ pour tout $n \in \mathbb N$.)
    \end{enumerate}
    \item
    \begin{enumerate}
      \item Soient $x,y \in \mathbb C$. On a alors $d_u(f(x),f(y))=|x+1-(y+1)|=|x-y|\leq d_s(x,y)$. L'application $f$ est donc $1$-lipschitzienne.
      \item Considérons la suite définie par $z_n=\frac{i}{n}$, $n\geq 1$. Puisque $d_s(0,z_n)=|0|+|\frac{i}{n}|=\frac{1}{n}\underset{n}{\rightarrow} 0$, la suite $\suiteN{z_n}$ tend vers $0$. Mais la suite $\suiteN{f(z_n)}$ ne tend pas vers $f(0)=1$. En effet, pour tout $n \geq 1$, $d_s(f(z_n),1)=d_s(z_n+1,1)=|z_n+1| +|1| \geq 2$, donc $\suiteN{f(z_n)}$ ne tend pas vers $1$ (cette suite $\suiteN{f(z_n)}$ ne converge pas, par exemple puisqu'elle n'est pas de Cauchy: $d_s(z_n+1,z_m+1)\geq 2$ si $n \neq m$). L'application $f$ n'est donc pas continue. (On peut également constater que l'image réciproque de l'ouvert $B_{d_s}(1,1)=]0,2[$ est $]-1,1[$, qui n'est pas ouvert (pour $d_s$), car aucune boule ouverte $B_{d_s}(0,r)=\{z,|z|<r\}$, $r>0$, n'est incluse dans $]-1,1[$.)
    \end{enumerate}
  \end{enumerate}
\end{solution}

\end{document}
