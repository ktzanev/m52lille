\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M52}

% \solutionstrue

\begin{document}

% ==================================
\hautdepage{
\sisujet{DS2}\sisolutions{Solutions du DS2}\\
  \normalfont\normalsize
 8 janvier 2020\\
  {[ durée: 3 heures ]}
}

% ==================================
\sisujet{
  \attention~\emph{Les documents ne sont pas autorisés. Le sujet comporte 4 exercices qui pourront être traités dans l'ordre de votre choix.}
  \bigskip
  \tsvp
}
% ==================================


%-----------------------------------
\begin{exo} \emph{(Questions de cours)}

  Les affirmations suivantes sont-elles vraies ou fausses? Justifier.
  \begin{enumerate}
    \item $A=]-1,1[$ est un ouvert dans $(\mathbb{C},\abs{})$.
    \item $B=[1,2[$ est un fermé de $(\mathbb{R},\abs{})$.
    \item $C=\ensemble{(\sin(t),\cos(t),e^t)}{t\in [0,1]}$ est un compact de $\mathbb{R}^3$ \emph{(muni de la norme euclidienne)}.
    \item La frontière d'une partie non vide d'un espace métrique est non vide.
    \item Soit $n\geq 1$. Sur $\mathbb{R}_n[X]$, les polynômes de degré au plus $n$, on considère les deux normes définies pour $p\in \mathbb{R}_n[X]$ par
    \[
      \norm{p}_\infty=\sup_{t\in [0,1]}|p(t)|\quad\text{et}\quad \norm{p}_1=\int_0^1 |p(t)|\dd t.
    \]
    Alors il existe deux constantes $c_1,c_2>0$ telles que pour tout $p\in\mathbb{R}_n[X]$, on a
    \[
      c_1 \norm{p}_\infty\leq \norm{p}_1\leq c_2 \norm{p}_\infty.
    \]
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item L'affirmation est fausse : $A=]-1,1[$ n'est pas un ouvert dans $(\mathbb{C},\abs{})$.  En effet, raisonnons par l'absurde et supposons que $A$ soit un ouvert de $(\mathbb{C},\abs{})$. Alors, comme $0\in A$, il existe $r>0$ tel que la boule ouverte $B(0,r)$ de centre $0$ et de rayon $r$ (dans $\mathbb C$) est contenue dans $A$. Mais ceci est absurde car par exemple $\frac{ir}{2}\in B(0,r)$ et $\frac{ir}{2}\not\in A$.
    \item L'affirmation est fausse: $B=[1,2[$ n'est pas un fermé de $(\mathbb{R},\abs{})$. En effet, raisonnons aussi par l'absurde et supposons que $B$ soit un fermé de $(\mathbb{R},\abs{})$. Considérons alors la suite $(u_n)_{n\geq 1}$ définie par $u_n=2-\frac{1}{n}$, $n\geq 1$. Alors pour tout $n\geq 1$, on a $u_n\in B$ et $u_n\xrightarrow{n\to +\infty} 2$. Mais si $B$ était fermé alors $2$ appartiendrait à $B$, ce qui n'est pas.
    \item L'affirmation est vraie : $C$ est un compact de $\mathbb R^3$. En effet, remarquons que l'application
    \[
      \begin{array}{rccl}
        \varphi:&[0,1]&\longrightarrow & \mathbb R^3\\
        &t&\longmapsto& \varphi(t)=(\sin(t),\cos(t),e^t)
      \end{array}
    \]
    est continue et $[0,1]$ est un compact. Or l'image d'un compact par une application continue est un compact. Ainsi, l'image de $\varphi$ est un compact de $\mathbb R^3$. Donc $C=\varphi([0,1])$ est un compact de $\mathbb R^3$.
    \item L'affirmation est fausse : la frontière d'une partie non vide d'un espace métrique n'est pas toujours non vide. Par exemple la frontière de l'espace tout entier est vide.
    \item L'affirmation est vraie. En effet, remarquons que le $\mathbb R$-espace vectoriel $\mathbb{R}_n[X]$ est de dimension finie
    (de dimension $n+1$). Or d'après le cours, on sait que si $E$ est un $\mathbb R$-espace vectoriel normé de dimension finie, alors toutes les normes sont équivalentes. On en déduit donc que les normes $\|\cdot\|_\infty$ et $\|\cdot\|_1$ sont équivalentes sur $\mathbb{R}_n[X]$ et donc il existe deux constantes $c_1,c_2>0$ telles que pour tout $p\in\mathbb{R}_n[X]$, on a
    \[
      c_1 \norm{p}_\infty\leq \norm{p}_1\leq c_2 \norm{p}_\infty.
    \]
  \end{enumerate}
\end{solution}

%-----------------------------------
\begin{exo}\emph{(Normes fonctionnelles)}

  Soit $E=C^1([0,1],\mathbb{R})$ le $\mathbb{R}$-espace vectoriel des applications de $[0,1]$ dans $\mathbb{R}$ qui sont de classe $C^1$ \emph{(c.-à-d. dérivables et à dérivée continue sur $[0,1]$)}. On considère sur $E$ les deux normes suivantes \emph{(on ne demande pas de vérifier que ce sont bien des normes)} :
  \[
    N_1(f)=|f(0)|+\|f'\|_\infty \quad\text{et}\quad N_2(f)=\|f\|_\infty+\|f'\|_\infty,
  \]
  où $\|g\|_\infty=\sup_{t\in [0,1]}|g(t)|$.
  \begin{enumerate}
    \item Montrer que $N_1$ et $N_2$ sont équivalentes.
    \item Sont-elles équivalentes à la norme $N(f)=\|f\|_\infty$? Justifier.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    \item Soit $f\in E$. Tout d'abord, il est clair que $|f(0)|\leq \|f\|_\infty$, ce qui donne immédiatement
    \[
      N_1(f)\leq N_2(f).
    \]
    D'autre part, pour $t\in [0,1]$, remarquons que
    \[
      f(t)=f(0)+\int_0^t f'(u)\,du,
    \]
    ce qui donne
    \[
      |f(t)|\leq |f(0)|+\int_0^t |f'(u)|\,du\leq |f(0)|+t \|f'\|_\infty\leq |f(0)|+\|f'\|_\infty=N_1(f)
    \]
    On a donc montré que pour tout $t\in [0,1]$, on a $|f(t)|\leq N_1(f)$, ce qui implique que $\|f\|_\infty\leq N_1(f)$. Comme d'autre part, on a clairement $\|f'\|_\infty\leq N_1(f)$, on en déduit que $N_2(f)=\|f\|_\infty+\|f'\|_\infty\leq 2 N_1(f)$. Ainsi, on a montré que pour tout $f\in E$, on a
    \[
      N_1(f)\leq N_2(f)\leq 2 N_1(f),
    \]
    ce qui montre que $N_1$ et $N_2$ sont équivalentes.
    \item Les deux normes $N_1$ et $N_2$ ne sont pas équivalentes à la norme $N$. En effet, supposons par l'absurde que $N_1$ est équivalente à la norme $N$. Alors il existerait une constante $c>0$ telle que pour toute fonction $f\in E$, on aurait
    \[
      N_1(f)\leq c N(f).
    \]
    En particulier, pour $n\geq 1$, considérons $f_n(t)=t^n$, $t\in [0,1]$. Alors $f'_n(t)=nt^{n-1}$. On remarque alors que $N_1(f_n)=|f_n(0)|+\|f_n'\|_\infty=n$ et $N(f_n)=\|f_n\|_\infty=1$ ce qui donnerait $n\leq c$. On obtient donc une contradiction en faisant tendre $n$ vers l'infini. Ainsi $N_1$ et $N$ ne sont pas équivalentes. Comme $N_2$ est équivalente à $N_1$, on en déduit que $N_2$ n'est pas équivalente à $N$ (par transitivité de la relation d'équivalence des normes).
  \end{enumerate}
\end{solution}


%-----------------------------------
\begin{exo} \emph{(Complétude, compacité et homéomorphismes)}

  Soient $(X,d_X)$ et $(Y,d_Y)$ deux espaces métriques et $f:X\longrightarrow Y$ une bijection.
  \begin{enumerate}
    \item Dans cette question, on suppose que $(Y,d_Y)$ est complet, que $f$ est uniformément continue et que $f^{-1}$ est continue. Montrer alors que $(X,d_X)$ est complet.
    \item On suppose maintenant que $(X,d_X)$ est compact et que $f$ est continue.
    \begin{enumerate}
    \item Soit $A\subset X$. On notera $g=f^{-1}:Y\longrightarrow X$. Justifier que $g^{-1}(A)=f(A)$ où $g^{-1}(A)$ désigne l'image réciproque de $A$ par $g$.
    \item Montrer que $f^{-1}$ est continue.
    \item Peut affirmer que $(Y,d_Y)$ est complet ? Justifier.
    \end{enumerate}
 \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
  \item Soit $\suite[index=n\geq 1]{x_n}$ une suite de Cauchy dans $(X,d_X)$ et $\varepsilon>0$. Comme $f$ est uniformément continue, il existe $\delta>0$ tel que
  \begin{equation}\label{eq:unc}
    d_X(x,y)<\delta\Longrightarrow d_Y(f(x),f(y))<\varepsilon.
  \end{equation}
  Comme $\suite[index=n\geq 1]{x_n}$ est une suite de Cauchy, il existe $N\in\mathbb N^*$ tel que
  \[
  n,m\geq N\Longrightarrow d_X(x_n,x_m)<\delta.
  \]
  Alors par \eqref{eq:unc}, on en déduit que
  \[
  n,m\geq N\Longrightarrow d_Y(f(x_n),f(x_m))<\varepsilon.
  \]
  Ainsi, on obtient que la suite $\suite[index=n\geq 1,size=\big]{f(x_n)}$ est une suite de Cauchy dans $(Y,d_Y)$ qui est complet. On en déduit donc qu'il existe $y\in Y$ tel que $f(x_n)\xrightarrow{n\to +\infty} y$ dans $(Y,d_Y)$. En utilisant maintenant le fait que $f^{-1}$ est continue, on obtient alors que $x_n=f^{-1}(f(x_n))\xrightarrow{n\to +\infty} f^{-1}(y)$ dans $(X,d_X)$. On a donc montré que si $\suite[index=n\geq 1]{x_n}$ est une suite de Cauchy dans $(X,d_X)$, alors elle converge. Ceci montre donc que $(X,d_X)$ est complet.
  \item
  \begin{enumerate}
    \item Par définition, on a $g^{-1}(A)=\ensemble{y\in Y}{g(y)\in A}=\ensemble{y\in Y}{f^{-1}(y)\in A}$. Comme $f$ est bijective, on a $f^{-1}(y)\in A$ si et seulement si $y\in f(A)$. Ainsi $g^{-1}(A)=\ensemble{y\in Y}{y\in f(A)}=f(A)$.
    \item D'après un résultat du cours, rappelons que $g=f^{-1}:Y\longrightarrow X$ est continue si et seulement si pour tout fermé $F$ de $X$, alors $g^{-1}(F)$ est un fermé de $Y$. Soit donc $F$ un fermé de $X$. D'après (i), on a  $g^{-1}(F)=f(F)$. Or $F$ étant fermé dans $X$ qui est compact, on en déduit que $F$ est compact. De plus, $f$ est continue et l'image d'un compact par une application continue est un compact. Ainsi, $f(F)$ est un compact de $Y$, donc en particulier $g^{-1}(F)=f(F)$ est un fermé de $Y$. On conclut finalement que $g=f^{-1}$ est continue.
    \item L'espace $(Y,d_Y)$ est complet. En effet, comme $f$ est une bijection, on a $Y=f(X)$ et comme l'image d'un compact par une application continue est un compact, on en déduit que $Y$ est compact. Or un compact est complet. Ainsi $Y$ est complet.
  \end{enumerate}
  \end{enumerate}
\end{solution}


%-----------------------------------
\begin{exo} \emph{(Partie connexe non connexe par arcs)}
  Dans le plan vectoriel euclidien $\mathbb{R}^2$ on considère $A=]0,1]\times \{0\} \cup \bigcup_{n\geqslant 1} \left(\{\frac{1}{n}\}\times [0,1]\right)$ et $A'=\{(0,1)\}$. On pose $B=A \cup A'$.
  \begin{enumerate}
    \item Représenter graphiquement $A$ et $A'$.
    \item Montrer que $B \subset \overline{A}$.
    \item Montrer que $A$ est connexe par arcs et que $B$ est connexe.
    \item On souhaite montrer que $B$ n'est pas connexe par arcs.\\
          Soit $\gamma \colon [0,1] \to B$ une application continue telle que $\gamma(0)=(0,1)$ et $\gamma(1)=(1,0)$. On note, pour tout $t \in [0,1]$, $\gamma(t)=\big(\gamma_x(t),\gamma_y(t)\big)$.
    \begin{enumerate}
      \item Montrer que $t_\ast \coloneqq \sup \gamma^{-1}(A') \in [0,1[$.
      \item Montrer qu'il existe $\delta > 0$ tel que, pour tout $t \in ]t_\ast,t_\ast + \delta [$, $\gamma_x(t) > 0$ et $\gamma_y(t) > 0$.
      \item En déduire que $\gamma_x(t) \in \ensemble{\frac{1}{n}}{n \in \mathbb{N}^*}$ pour $t \in ]t_\ast,t_\ast + \delta[$, puis aboutir à une contradiction et conclure.
    \end{enumerate}
  \end{enumerate}
\end{exo}

\begin{solution}
  \sidebyside{.7}{
    \begin{enumerate}
      \item \emph{Voir l'image ci-contre.}
      \item Soit $x_{n}=(\frac{1}{n},1) \in A$ pour $n \in \mathbb{N}^{*}$, nous avons $x_{n} \xrightarrow{n\to\infty} (0,1)$ et donc $A' \subset \overline{A}$. Et comme de plus $A \subset \overline{A}$ par définition, nous avons $B \subset \overline{A}$.
      \item Tout point $(\lambda,0)$ de $]0,1]\times \{0\}$ est connecter par un chemin rectiligne, dans $A$, à $(1,0)$. Tout point $(\frac{1}{n},\lambda)$ de $\{\frac{1}{n}\}\times [0,1]$ est connecter par un chemin rectiligne, dans $A$, à $(\frac{1}{n},0) \in ]0,1]\times \{0\}$. Ainsi tout point de $A$ est connecter par chemin dans $A$ à $(0,1) \in A$. En conclusion $A$ est connexe par chemins, donc $A$ est connexe et donc $B$ est connexe car il vérifie $A\subset B \subset \overline{A}$.
    \end{enumerate}
  }{
    ~\\[-7mm]\includegraphics{M52_2019-20-DS2_img_exo4a.pdf}
  }
  \begin{enumerate}[start=4]
    \item
    \begin{enumerate}
      \item Comme $\gamma$ est continue et $A'$ est fermé non vide on trouve que $\gamma^{-1}(A')$ est un fermé non vide de $[0,1]$ donc un compact non vide. Ainsi nous avons $t_\ast=\max \gamma^{-1}(A') \in \gamma^{-1}(A') \subset [0,1]$ et comme $\gamma(1)=(1,0) \notin A'$ nous avons $t_\ast \in [0,1[$.
      \item Comme $\gamma_{y}$ est continue et $\gamma_{y}(t_\ast) = 1$ nous avons qu'il existe $\delta > 0$ tel que, pour tout $t \in ]t_\ast,t_\ast + \delta [$, $\gamma_y(t) > 0$. De plus pour tout $t \in ]t_\ast,t_\ast + \delta [$ nous avons $t>t_{\ast}=\max\gamma^{-1}(A')$ $\implies$ $\gamma(t) \notin A'$ $\implies$ $\gamma(t) \in A$ $\implies$ $\gamma_x(t) \in ]0,1]$.
      \item D'après la question précédente $\gamma(t) \in \bigcup_{n\geqslant 1} \left(\{\frac{1}{n}\}\times [0,1]\right)$ pour $t \in ]t_\ast,t_\ast + \delta [$ car $\gamma_x(t) > 0 \implies \gamma(t) \notin A'$ et $\gamma_y(t) > 0 \implies \gamma(t) \notin ]0,1]\times \{0\}$. Ainsi d'une part, $\gamma_x(]t_\ast,t_\ast + \delta [) \subset \ensemble{\frac{1}{n}}{n \in \mathbb{N}^*}$ est dénombrable, et d'autre part, l'image de intervalle $]t_\ast,t_\ast + \delta [$ par l'application continue $\gamma_x$ est un intervalle. Mais un intervalle non vide est dénombrable si et seulement s'il est réduit à un point, par exemple $\{\frac{1}{n}\}$. Ainsi on trouve que $\gamma_x\equiv \frac{1}{n}$, pour un certain $n \in \mathbb{N}^{*}$ fixé, sur $]t_\ast,t_\ast + \delta [$. Ce qui est en contradiction avec $\gamma_{x}(t)\xrightarrow{t\to t_\ast} \gamma_{x}(t_\ast)=0$. En conclusion notre supposition est fausse : il n'existe pas de chemin dans $B$ qui relie $(0,1)$ à $(1,0)$, donc $B$ n'est pas connexe par chemins (même s'il est connexe).
    \end{enumerate}
  \end{enumerate}
\end{solution}

\end{document}
