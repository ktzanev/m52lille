# [m52lille](https://gitlab.com/ktzanev/m52lille) / [page web](https://ktzanev.gitlab.io/m52lille/)

Les tds du module M52 - «Topologie» de l'Université de Lille

## 2019/20

Vous pouvez récupérer [ce dépôt](https://gitlab.com/ktzanev/m52lille) de deux façons faciles :

- en téléchargeant l'archive [zip](https://gitlab.com/ktzanev/m52lille/-/archive/master/m52lille-master.zip) qui contient la dernière version des fichiers,
- récupérer l'intégralité de ce dépôt, y compris l'historique des modifications, en utilisant `git` avec la commande

  ~~~~~~~
  git clone git@gitlab.com:ktzanev/m52lille.git
  ~~~~~~~

Dans [ce dépôt](https://gitlab.com/ktzanev/m52lille) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX) suivants :

- Feuille de TD n°1 [[tex](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD1.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD1.pdf?inline=false)]
- Feuille de TD n°2 [[tex](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD2.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD2.pdf?inline=false)]
- Feuille de TD n°3 [[tex](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD3.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD3.pdf?inline=false)]
- Feuille de TD n°4 [[tex](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD4.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD4.pdf?inline=false)]
- Feuille de TD n°5 [[tex](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD5.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD5.pdf?inline=false)]
- Feuille de TD n°6 [[tex](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD6.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52_2019-20_TD6.pdf?inline=false)]
- DS1 [[tex](https://gitlab.com/ktzanev/m52lille/raw/master/Examens/M52_2019-20_DS1.tex?inline=false)] [sujet [pdf](https://gitlab.com/ktzanev/m52lille/raw/master/Examens/M52_2019-20_DS1_sujet.pdf?inline=false)] [solutions [pdf](https://gitlab.com/ktzanev/m52lille/raw/master/Examens/M52_2019-20_DS1_solutions.pdf?inline=false)]
- DS2 [[tex](https://gitlab.com/ktzanev/m52lille/raw/master/Examens/M52_2019-20_DS2.tex?inline=false)] [sujet [pdf](https://gitlab.com/ktzanev/m52lille/raw/master/Examens/M52_2019-20_DS2_sujet.pdf?inline=false)] [solutions [pdf](https://gitlab.com/ktzanev/m52lille/raw/master/Examens/M52_2019-20_DS2_solutions.pdf?inline=false)]

Pour compiler ces feuilles de td vous avez besoin de la feuille de style [M52.sty](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/M52.sty?inline=false) ainsi que le [logo du déprtement](https://gitlab.com/ktzanev/m52lille/raw/master/TDs/ul-fst-math_noir.pdf?inline=false).

## 2018/19

Vous pouvez téléchargeant la version 2018/19 en [zip](https://gitlab.com/ktzanev/m52lille/-/archive/v2018/m52lille-v2018.zip) qui contient la version des fichiers de cette année.

Dans [ce dépôt](https://gitlab.com/ktzanev/m52lille/tree/v2018) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX) suivants :

- Feuille de TD n°1 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD1.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD1.pdf?inline=false)]
- Feuille de TD n°2 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD2.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD2.pdf?inline=false)]
- Feuille de TD n°3 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD3.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD3.pdf?inline=false)]
- Feuille de TD n°4 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD4.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD4.pdf?inline=false)]
- Feuille de TD n°5 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD5.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD5.pdf?inline=false)]
- Feuille de TD n°6 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD6.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD6.pdf?inline=false)]
- Feuille de TD n°7 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD7.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD7.pdf?inline=false)] *(non faite)*
- Feuille de TD n°8 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD8.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52_2018-19_TD8.pdf?inline=false)] *(non faite)*
- DS1 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/Examens/M52_2018-19_DS1.tex?inline=false)] [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/Examens/M52_2018-19_DS1.pdf?inline=false)]
- DS2 [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/Examens/M52_2018-19_DS2.tex?inline=false)], sujet [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/Examens/M52_2018-19_DS2_sujet.pdf?inline=false)]  et correction [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/Examens/M52_2018-19_DS2_solutions.pdf?inline=false)]
- Rattrapage [[tex](https://gitlab.com/ktzanev/m52lille/raw/v2018/Examens/M52_2018-19_Rattrapage.tex?inline=false)], sujet [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/Examens/M52_2018-19_Rattrapage_sujet.pdf?inline=false)]  et correction [[pdf](https://gitlab.com/ktzanev/m52lille/raw/v2018/Examens/M52_2018-19_Rattrapage_solutions.pdf?inline=false)]


Pour compiler ces feuilles de td vous avez besoin de la feuille de style [M52.sty](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/M52.sty?inline=false) ainsi que le [logo du déprtement](https://gitlab.com/ktzanev/m52lille/raw/v2018/TDs/ul-fst-math_noir.pdf?inline=false).
