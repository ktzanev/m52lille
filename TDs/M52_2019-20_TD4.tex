\documentclass[a4paper,10pt,reqno]{amsart}
\usepackage{M52}

\begin{document}

\hautdepage{TD4 - Espaces complets}


% ==================================
\section{Suites de Cauchy}
% ==================================


% -----------------------------------
\begin{exo}[.7]

  Soit $(E,d)$ un espace métrique et $(x_n)_{n\in\mathbb{N}}$ une suite de points de $E$. On pose $A_n=\{x_n,x_{n+1},\ldots\}$.\\
  Montrer que $(x_n)_{n\in\mathbb{N}}$ est une suite de Cauchy si et seulement si $\lim_{n\to +\infty} \diam(A_n)=0$.
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soit $(E,d)$ un espace métrique et $(x_n)_{n\in\mathbb{N}}$ est une suite de Cauchy de $E$. Montrer que s'il existe une sous-suite $(x_{n_k})$ qui admet une limite $l \in E$, alors $(x_n)$ converge vers $l$.
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Décrire les suites de Cauchy dans un espace $E$ muni de la métrique discrète et montrer qu'un tel espace est complet.
\end{exo}


% -----------------------------------
\begin{exo}

  Déterminer si les suites $(u_n)_{n>0}$ suivantes sont de Cauchy:
  \begin{enumerate}
    \item $u_n=(-1)^n$ dans $(\mathbb{R}, \vert\cdot\vert)$;
    \item $u_n=(n\sin \frac1n,\cos\frac{1}{n})$ dans $(\mathbb{R}^2,\Vert\cdot\Vert_2)$;
    \item $u_n$ dans  $(\C{[0,1];\mathbb{R}},\Vert\cdot\Vert_1)$, avec $u_n(t)=n-n^2\vert t\vert$ si $n\vert t\vert\le 1$ et $u_n(t)=0$ sinon.
  \end{enumerate}
\end{exo}


% ==================================
\section{Espaces complets}
% ==================================


% -----------------------------------
\begin{exo}

  Soit $\mathbb{R}$ muni de la distance $d(x,y)=|\arctan x-\arctan y|$.
  \begin{enumerate}
    \item Montrer que pour tous $x,y\in\mathbb{R}$, $d(x,y)\leq |x-y|$.
    \item Montrer que $d$ et $|\cdot|$ sont topologiquement équivalentes.
    \item En considérant la suite $(u_n)$ définie pour tout $n$ par $u_n=n$, montrer que $(\mathbb{R},d)$ n'est pas complet.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  \begin{enumerate}
    \item Montrer que $(\mathbb{R}_+,|\cdot|)$ est complet et que $f:x\mapsto\frac{x}{1-x}$ est un homéomorphisme de $[0;1[$ sur $\mathbb{R}_+$.
    \item Vérifier que la suite $(u_n)$, avec $u_n=\tanh(n)$, est de Cauchy mais ne converge pas dans $[0;1[$. La suite $\bigl(f(u_n)\bigr)$ est-elle de Cauchy dans $\mathbb{R}_+$?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  On munit $\mathbb{R}^2$ de la norme euclidienne $\Vert\cdot\Vert_2$.
  Soit $A=B(0,1)$. On considère deux distances sur $A$: $d_2(x,y)=\|x-y\|_2$ induite par la norme euclidienne, et
  $\displaystyle d_1(x,y)=\|x-y\|_2+\left|\frac{1}{d_2(x,A^c)}-\frac{1}{d_2(y,A^c)}\right|$.
  \begin{enumerate}[a)]
    \item Vérifier que $d_1$ est une distance.
    \item Montrer que l'identité $\operatorname{Id}: (A,d_1)\to(A,d_2)$ est un homéomorphisme.
    \item Montrer que $(A,d_2)$ n'est pas complet, mais que $(A,d_1)$ est complet.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $f:\mathbb{R}\to\mathbb{R}$ une fonction continue, $\Gamma:=\ensemble{(x,f(x))}{x\in\mathbb{R}}$ son graphe dans $\mathbb{R}^2$ euclidien.
  \begin{enumerate}
    \item Montrer que $\Gamma$ est complet.
    \item Est-ce encore vrai si $f$ n'est pas continue ?
    \item Est-ce que la complétude de $\Gamma$ implique la continuité de $f$ ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $E$ l'ensemble des fonctions lipschitziennes de $[0;1]$ dans $\mathbb{R}$. Pour
  $f\in E$, on pose
  \[
    N(f)=\sup_{x\in [0;1]}|f(x)|+\sup_{x\not= y}\frac{|f(x)-f(y)|}{|x-y|}.
  \]
  Montrer que $N$ est une norme sur $E$, pour laquelle $E$ est complet.
\end{exo}


% -----------------------------------
\begin{exo}

  On note $\ell^\infty(\mathbb{R})$ l'ensemble des suites réelles bornées que l'on munit de la norme $\Vert\cdot\Vert_\infty$. Soit aussi $c_0(\mathbb{R})$ l'ensemble des suites réelles qui tendent vers 0.
  \begin{enumerate}
    \item Montrer que $(\ell^\infty(\mathbb{R}),\Vert\cdot\Vert_\infty)$ est complet.
    \item En déduire que $(c_0(\mathbb{R}),\Vert\cdot\Vert_\infty)$ est complet.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.49]

  Montrer que $\ell^p(\mathbb K)$ est un espace de Banach, pour tout $1\leq p\leq \infty$.
\end{exo}


% ==================================
\section{Théorème du point fixe}
% ==================================


% -----------------------------------
\begin{exo}[.7]\label{exo:contr}

  Soit $(E,d)$ un espace métrique complet, $f$ une application de $E$ dans $E$. On suppose qu'il existe $n\in\mathbb{N}^*$ tel que la $n$-ième itérée $f^n=f\circ\cdots\circ f$ soit strictement contractante. Montrer que $f$ a un unique point fixe dans $E$.
\end{exo}


% -----------------------------------
\begin{exo}

  On cherche les solutions $f\in\C[1]{[0,1],\mathbb R}$ du système
  \begin{equation}\tag*{$\circledast$}\label{equadiff}
    \begin{cases}
      f'(x) = f(1-x^2)\\
      f(0)  = 1
    \end{cases}.
  \end{equation}
  Soit $E = \C{[0,1],\mathbb R}$ muni de la norme $\norm{}_{\infty}$.
  \begin{enumerate}
    \item Montrer qu'il existe une unique fonction $f_{*}\in E$ qui est le point fixe de l'opérateur $T$ défini par
    \[
      T(f)(x)=1+\int_0^x f(1-t^2)\,dt.
    \]
    \begin{indication}
      On pourra montrer que $T^2=T\circ T$ est une application strictement contractante et appliquer le résultat de l'exercice \ref{exo:contr}.
    \end{indication}
    \item Montrer que $f_{*}\in\C[\infty]{[0,1],\mathbb R}$.
    \item Conclure que $f_{*}$ est l'unique solution de \ref{equadiff}.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soit $a\in\mathbb{R}$ et $\lambda\in\mathbb{C}$ avec $|\lambda|<1$. Montrer que, pour toute fonction bornée $g \in \C{\mathbb{R};\mathbb{C}}$, il existe une unique fonction bornée $f \in \C{\mathbb{R};\mathbb{C}}$ telle que $\forall x\in\mathbb{R},\ f(x)-\lambda f(x+a)=g(x)$.
\end{exo}


% ==================================
\section{Compléments de cours}
% ==================================


% -----------------------------------
\begin{exo} \emph{(Théorème de Baire)}

  Soit $(A_n)_{n}$ une suite d'ouverts denses dans un espace métrique complet $(E,d)$.
  \begin{enumerate}
    \item Soit $x_0\in E$ et $r_0>0$.
    \begin{enumerate}
      \item Montrer qu'il existe deux suites $(x_n)_{n\in\mathbb{N}}\in E^\mathbb{N}$ et $(r_n)_{n\in\mathbb{N}}\in\mathbb{R}^\mathbb{N}$ telles que
      \begin{enumerate}
        \item Pour tout $n\geq 0$, $\fermeture{B(x_{n+1},r_{n+1})}\subset B(x_{n},r_{n})\cap A_n$,
        \item Pour tout $n\geq 1$, $0<r_n<\frac1{2^n}$.
      \end{enumerate}
      \item Montrer que $(x_n)_n$ est une suite de Cauchy. En déduire qu'elle converge. On note $x_*$ sa limite.
      \item Montrer que quels que soient $p,q\in\mathbb{N}$, $p>q$, $x_p$ appartient à ${B(x_q,r_q)}$. En déduire que $x_*$ appartient à $B(x_0,r_0)\cap \bigcap_{n\geq 1} A_n$.
      \item  Montrer que $\bigcap_{n\geq 1} A_n$ est dense dans $E$
    \end{enumerate}
    \item Soit $X\subset\mathbb{R}$ un fermé dénombrable non vide. Montrer que $X$ a au moins un point isolé (on pourra raisonner par l'absurde et considérer les ouverts $\omega_x=X\setminus\{x\}$ de $X$).
    \item Soit $(F_n)_n$ une suite de fermés d'intérieur vide de $E$. Montrer que $\bigcup_{n\geq 1} F_n$ est d'intérieur vide.
    \item Montrer que $(\mathbb{R}[X],\|\cdot\|)$, quelle que soit la norme $\|\cdot\|$ considérée, n'est jamais complet (on pourra regarder la suite de fermés $\big(\mathbb{R}_n[X]\big)_n$ de $\mathbb{R}[X]$).
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} \emph{(Caractérisation des espaces de Banach)}

  Soit $(X,\|\cdot\|)$ un espace vectoriel normé. On a vu en cours que si $(X,\|\cdot\|)$ est un espace de Banach, alors toute série absolument convergente est convergente. Le but de l'exercice est de montrer la réciproque, à savoir que  si toute série absolument convergente est convergente, alors $(X,\|\cdot\|)$ est un espace de Banach.

  On suppose que toute série absolument convergente converge. Soit $\left(x_n\right)_n\subset X$ une suite de Cauchy.
  \begin{enumerate}
    \item Construire une suite extraite $\left(x_{n_k}\right)_k$ tel que pour tout $k$, $y_k=x_{n_{k+1}}-x_{n_k}$ satisfait $\|y_k\|<\frac1{2^k}$.
    \item En déduire que $\sum_{k\geq 0} y_k$, puis que $\left(x_n\right)_n$, convergent.
  \end{enumerate}
\end{exo}


\end{document}

