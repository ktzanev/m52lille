\documentclass[a4paper,10pt,reqno]{amsart}
\usepackage{M52}

\begin{document}

\hautdepage{TD1 - Normes et métriques}

% ==================================
\section{Espaces vectoriels normés}
% ==================================

\begin{convention}
  Dans la suite $\mathbb{K}$ désigne le corps $\mathbb{R}$ ou $\mathbb{C}$.
\end{convention}

% -----------------------------------
\begin{exo}

  Soit $1 \leq p \leq \infty$ et $q$ tel que $\frac{1}{p} + \frac{1}{q} = 1$ \emph{(on dit que $p$ et $q$ sont conjugués avec la convention que $1$ et $\infty$ sont conjugués)}. On rappelle que pour $a = (a_1,a_2,\dots,a_n) \in \mathbb{K}^n$, on note
  \[
    \|a\|_p=\left(\sum_{i=1}^n |a_i|^p\right)^{1/p},\qquad 1\leq p<\infty,
  \]
  et
  \[
    \|a\|_\infty=\max_{1\leq i\leq n}|a_i|.
  \]
  Le but de l'exercice est de montrer que $(\mathbb{K}^n,\|\cdot\|_p)$ est un $\mathbb{K}$-espace vectoriel normé.
  \begin{enumerate}
    \item Vérifier que pour $p=1$ et pour $p=\infty$, $(\mathbb{K}^n,\|\cdot\|_p)$ est un $\mathbb{K}$-espace vectoriel normé.
  \end{enumerate}
  \emph{On supposera dans la suite que $1<p<\infty$.}
  \begin{enumerate}[resume]
    \item Montrer que pour tout $a,b\geq 0$, on a
    \[
      ab\leq \frac{a^p}{p}+\frac{b^q}{q}\qquad \text{(Inégalité de Young)}.
    \]
    \item Montrer que pour tout $a=(a_1,a_2,\dots,a_n)$, $b=(b_1,b_2,\dots,b_n)\in \mathbb{K}^n$, on a
    \[
      \left|\sum_{i=1}^n a_ib_i\right|\leq \|a\|_p \|b\|_q\qquad \text{(Inégalité de Hölder)}.
    \]
    \begin{indication}
      Soit $A>0$. Écrire $|a_ib_i|=|Aa_i||b_i/A|$, appliquer l'inégalité de Young, puis sommer. Choisir alors $A$ tel que $A^p\|a\|_p^p=\|b\|_q^q/A^q$.
    \end{indication}
    \item Montrer que pour $1<p<\infty$, $(\mathbb{K}^n,\|\cdot\|_p)$ est un $\mathbb{K}$-espace vectoriel normé.\\
    \begin{indication}
      Appliquer l'inégalité de Hölder à \\
      $\sum_{i=1}^n |a_i||a_i+b_i|^{p-1}$ et à $\sum_{i=1}^n |b_i||a_i+b_i|^{p-1}$, puis sommer pour en déduire l'inégalité triangulaire pour $\|\cdot\|_p$.
    \end{indication}
  \end{enumerate}
\end{exo}



% -----------------------------------
\begin{exo}

  Pour $1\leq p\leq +\infty$, on définit $\ell^p(\mathbb{K})$ l'ensemble des suites $u=\suiteN{u_n}$ à valeurs dans $\mathbb{K}$ telles que
  \[
    \|u\|_p=\left(\sum_{n=0}^\infty|u_n|^p\right)^{1/p}<+\infty,\qquad 1\leq p<+\infty,
  \]
  et
  \[
    \|u\|_{\infty}=\sup_{n\geq 0}|u_n|<+\infty.
  \]
  Montrer que :
  \begin{enumerate}
    \item  $\ell^p(\mathbb{K})$ est un $\mathbb{K}$-espace vectoriel.
    \item  $\|\cdot\|_p$ est une norme sur $\ell^p(\mathbb{K})$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $-\infty<a<b<+\infty$ et $\C{[a,b]}$ l'espace des fonctions continues sur $[a,b]$ à valeurs dans $\mathbb{K}$. On définit pour $f\in \C{[a,b]}$ et $1\leq p\leq +\infty$,
  \[
    \|f\|_p=\left(\int_a^b |f(t)|^p\right)^{1/p}, \qquad 1\leq p<+\infty,
  \]
  et
  \[
    \|f\|_\infty=\sup_{t\in [a,b]}|f(t)|.
  \]
  Montrer que $\bigl( \C{[a,b]},\|\cdot\|_p \bigr)$ est un espace vectoriel normé.\\
  \begin{indication}
    On pourra s'inspirer du cas discret.
  \end{indication}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $\mathbb{R}^n$ munit de la norme $\|\cdot\|_p$.
  \begin{enumerate}
    \item Dessiner pour $n=2$ et pour $p=1,2,\infty$ la boule unité de $(\mathbb{R}^n,\|\cdot\|_p)$, c'est-à-dire
    \[
      B_p(0,1)=\left\{x\in\mathbb{R}^2:\|x\|_p\leq 1\right\}.
    \]
    \item Montrer que pour $x \in \mathbb{R}^n$ et $p > q \geq 1$, on  a
    \[
      \| x \|_q n^{\frac{1}{p}-\frac{1}{q}} \leq \|x\|_p \leq \|x\|_q.
    \]
  \end{enumerate}
  Que peut-on en déduire des normes $\|\cdot\|_p$ sur $\mathbb{R}^n$?
\end{exo}



% -----------------------------------
\begin{exo}

  Soient $n \geq 0$ et $|\cdot|$ une norme sur le $\mathbb{K}$-espace vectoriel $\mathbb{K}^n$.
  Pour $M \in \mathcal{M}_n(\mathbb{K})$, on note
  \[
    \|M\|\ \coloneqq \sup_{z\in \mathbb{K}^n, |z|\leq1} |Mz|.
  \]
  \begin{enumerate}
    \item Montrer que $\|\cdot\|$ définit une norme sur $\mathcal{M}_n(\mathbb{K})$.
    \item Montrer que $\|\cdot\|$ est une norme matricielle, c'est-à-dire, que pour $M,N\in \mathcal{M}_n(\mathbb{K})$, on a $\|MN\|\,\leq\, \|M\|\,\|N\|.$
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(E,\left<\cdot,\cdot\right>)$ un espace préhilbertien et $\|\cdot\|$ la norme associée.
  Montrer l'identité du parallélogramme :
  \[
    \|x+y\|^2+\|x-y\|^2=2\|x\|^2 + 2\|y\|^2.
  \]
  En déduire que si $\|x\|=\|y\|=1$ et $x\neq y$ alors $\|\frac{1}{2}(x+y)\|<1$. \\
  Montrer que sur $\mathbb{K}^n$ les normes $\|\cdot\|_\infty$ et $\|\cdot\|_1$ ne sont pas issues d'un produit scalaire.
\end{exo}



 % -----------------------------------
\begin{exo}

  Soit $(E,\|\cdot\|)$ un espace vectoriel normé. Pour tout $x_0\in E$ et tout $r>0$, on note $S(x_0,r)$ la sphère de centre $x_0$ et de rayon $r$. Soient $a,a'\in E$, $r,r'>0$ tels que $S(a,r)=S(a',r')$.
  \begin{enumerate}
    \item Montrer que si $a\neq a'$ alors $\|a-a'\|=r-r'$.
    \begin{indication}
      Considérer le point $p \in S(a,r)$ tel que $a' \in [a,p]$.
    \end{indication}
    \item De même, montrer que $\|a'-a\|=r'-r$.
    \item En déduire que $a=a'$ et $r=r'$.
  \end{enumerate}
\end{exo}


 % -----------------------------------
\begin{exo}

  On note $E$ l'espace vectoriel des fonctions $f:[0,1] \to \mathbb{R}$ de classe $C^1$ telles que $f(0) = 0$. Pour tout $f \in E$ on pose
  \begin{align*}
     N_1(f) &= \lVert f'(t)\rVert_\infty =\sup_{t\in[0,1]} |f'(t)|,\\
     N_2(f) &= \lVert f(t)\rVert_\infty + \lVert f'(t)\rVert_\infty.
  \end{align*}
  Montrer que $N_1$ et $N_2$ sont deux normes équivalentes sur $E$.
\end{exo}



 % -----------------------------------
\begin{exo}

  Dans l'espace vectoriel $E=\mathbb{R}[X]$ des polynômes à coefficients réels, pour tout polynôme $P(X)=a_0+a_1 X+\ldots+a_d X^d \in \mathbb{R}_{d}[X]$, on pose
  \[
    N(P)=|a_0+P'(1)|+\sum_{k=1}^d|a_k|  \text{ et } \tilde N(P)=\sup_{x\in [0,1]}|P(x)|.
  \]
  \begin{enumerate}
    \item Montrer que l'on a ainsi défini deux normes sur $E$.
    \item Soit la suite $\left(P_n\right)_{n \in \mathbb{N}}$ où $P_n(X)=1-\frac{X^n}n$. Montrer que cette suite converge vers le polynôme nul pour la norme $N$ et vers le polynôme 1 pour la norme $\tilde N$.
    \item Les normes $N$ et $\tilde N$ sont-elles équivalentes ?
  \end{enumerate}
\end{exo}


% ==================================
\section{Espaces métriques}
% ==================================


% -----------------------------------
\begin{exo}[.7]

  Montrer que si $f : X \rightarrow Y$ est une injection et $(Y,d)$ est un espace métrique, alors $\delta(x,y) = d\bigl(f(x),f(y)\bigr)$ est une métrique sur $X$.
\end{exo}


% -----------------------------------
\begin{exo}

  Vérifier que les espaces suivants sont des espaces métriques :
  \begin{enumerate}
    \item $\mathbb{R}^*$ avec $d(x,y)=\left|\frac1x-\frac1y\right|$,
    \item $\mathbb{R}$ avec $d(x,y)=\left|\arctan x-\arctan y\right|$,
    \item $\C{[0,1]}$ avec $d(f,g)=\sup_{[0,1]} |f-g|$,
    \item $\C{[0,1]}$ avec $d(f,g)=\int_0^1|f(x)-g(x)|dx$,
    \item $E=\{0,1\}^\mathbb{N}$ avec $d(u,v)=\sum_{n=0}^\infty \frac1{2^n}|u_n-v_n|$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(X_1,d_1),\dots, (X_n,d_n)$ des espaces métriques et $X=X_1\times\dots\times X_n$.  Pour $1\leq p\leq \infty$ et $x=(x_1,\dots,x_n), y=(y_1,\dots,y_n)\in X$,  on pose
  \[
    d_p(x,y)=\|(d_1(x_1,y_1),\dots,d_n(x_n,y_n))\|_p,
  \]
  où $\|\cdot\|_p$ est la norme sur $\mathbb{R}^n$ défini par
  \[
    \|(u_1,\dots,u_n)\|_p =
      \begin{cases}
        \max_{1\leq i\leq n}|u_i| & \text{si }p=\infty\\
        \left(\sum_{i=1}^n |u_i|^p\right)^{1/p}& \text{si }1\leq p<\infty.
      \end{cases}
  \]
  Montrer que $d_p$ définit une distance sur $X$.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(E,d)$ un espace métrique.
  \begin{enumerate}[a)]
    \item Montrer que $\overline{B(a,r)} \subset BF(a,r)$ et donner un exemple
    d'inclusion stricte \indic{(on pourra considérer par exemple $\{0\}\cup\{1\}$)}.
    \item Montrer qu'il y a égalité dans un $\mathbb{K}$-espace vectoriel normé.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  On munit $\mathbb{R}$ de la distance usuelle $d(x,y)=\vert x-y\vert $ et on pose $\delta (x,y)=\phi\bigl(d(x,y)\bigr)$ avec $\phi(t)=\frac t{1+t}$.
  \begin{enumerate}
    \item\label{q:delta} Vérifier que $\delta$ représente une distance dans $\mathbb{R}$.\\
    \begin{indication}
      Montrer et utiliser que $\phi$ est croissante et concave.
    \end{indication}
    \item\label{q:topeq} En précisant les ensembles $\{x\in\mathbb{R}:\delta(x,0)<r\}$ pour les $r>0$, démontrer que $d$ et $\delta$ sont topologiquement équivalentes.
    \item Démontrer que $d$ et $\delta$ ne sont pas deux distances équivalentes.
    \item Dire si les résultats des questions (\ref{q:delta}) et (\ref{q:topeq}) ci-dessus demeurent encore lorsque $\frac t{1+t}$ est remplacée par l'une des fonctions suivantes:\\[7pt]
    \begin{enumerate*}[itemjoin=\quad]
      \item $t\mapsto \ln(1+t)$,
      \item $t\mapsto \arctan t$,
      \item $t\mapsto t^2$.
    \end{enumerate*}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(X_1,d_1)$ et $(X_2,d_2)$ des espaces métriques et soit $\delta_1$, $\delta_2$ des distances de l'espace produit $X_1\times  X_2$ définies par: $\delta_1(x,y) = d_1(x_1,y_1) + d_2(x_2,y_2)$, $\delta_2(x,y) = \max \big(d_1(x_1,y_1),d_2(x_2,y_2) \big)$, où $x = (x_1,x_2)$, $y = (y_1,y_2)$. Démontrer que $\delta_1$ et $\delta_2$ sont deux distances équivalentes.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $\suite[j\ge 0]{X_j,d_j}$ une suite d'espaces métriques, $X=\prod_{j\ge 0}X_j$ et notons $d$ l'application définie de $ X\times X$ dans $\mathbb{R}$ par:
  \[
    d(x,y)=\sum_{j\ge 0}2^{-j}\,\frac{d_j(x_j,y_j)}{1+d_j(x_j,y_j)}\leqno(*)
  \]
  si $x=\suite[j\ge 0]{x_j},\ y=\suite[j\ge 0]{y_j}$.
  \begin{enumerate}
   \item Expliquer pourquoi la série définissant $d$ ci-dessus est convergente.
   \item Démontrer que $d$ est une métrique de $X$.
   \item Soit $\suite[k\ge 0]{x^{(k)}}$ une suite d'éléments de $X$, avec $x^{(k)}=\bigl(x^{(k)}_{j}\bigr)_{j\ge 0}$, $x_j^{(k)}\in X_j$. Établir l'équivalence entre les assertions suivantes:
   \begin{enumerate}
    \item Chaque suite «composante» $\bigl(x_j^{(k)}\bigr)_{k\ge 0}$ converge dans $(X_j,d_j)$.
    \item La suite $\suite[k\ge 0]{x^{(k)}}$ converge dans $(X,d)$.
   \end{enumerate}
  \item \emph{[généralisation]} Démontrer qu'à toute série numérique à termes strictement positifs et convergente $\sum_{j\ge 0}\lambda_j$ correspond une métrique $\delta$ obtenue en remplaçant dans $(*)$ les coefficients $2^{-j}$ par $\lambda_j$ et que la nouvelle distance est toujours topologiquement équivalente à $d$. Sont-elles des distances équivalentes ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(X,d)$ un espace métrique. On dit que $d$ est une distance \emph{ultramétrique} si elle vérifie l'inégalité triangulaire plus forte :
  \[
    d(x,z)\leq \max \big\{ d(x,y),d(y,z) \big\}.
  \]
  \begin{enumerate}
    \item Vérifier que  la distance discrète sur un ensemble $E$ quelconque est une distance ultramétrique.
    \item Soit $X$ un ensemble muni d'une distance ultramétrique. Vérifier les propriétés suivantes :
    \begin{enumerate}
      \item si deux boules ouvertes (fermées) ont un point commun, alors nécessairement l'une contient l'autre;
      \item pour tout $a\in X$, pour tout $r>0$ et pour tout $x\in B(a,r)$, on a $B(x,r)=B(a,r)$;
      \item toute boule fermée de rayon non nul est ouverte;
      \item toute boule ouverte est fermée;
      \item tout triangle est isocèle (autrement dit, étant donné trois points, les deux plus proches sont à la même distance du troisième).
    \end{enumerate}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $\mathbb K$ un corps. Une \emph{valeur absolue} sur $\mathbb K$ est une application, notée $|\cdot|$, définie sur $\mathbb K$ à valeurs dans $[0,\infty[$, et vérifiant, pour tous $x,y \in \mathbb K$, les trois axiomes suivants:
  \begin{itemize}[$\blacktriangleright$]
    \item $|x|=0\Leftrightarrow x=0_\mathbb K$,
    \item $|x+y|\leq |x|+|y|$,
    \item $|xy|=|x||y|$.
  \end{itemize}

  Montrer que l'application $(x,y)\mapsto |y-x|$ est une distance sur $\mathbb K$.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $p$ un nombre premier. On appelle valuation $p$-adique l'application $v_p:\mathbb{Q}\longrightarrow \mathbb{Z}\cup\{+\infty\}$ définie comme :
  \begin{itemize}
    \item $v_p(0)=+\infty$;
    \item si $n$ est un entier non nul, $v_p(n)=k$ si $p^k$ divise $n$ et $p^{k+1}$ ne divise pas $n$;
    \item si $n=\frac{a}{b}$ avec $a$ et $b$ des entiers non nuls, alors $v_p(n)=v_p(a)-v_p(b)$.
  \end{itemize}
  \begin{enumerate}
    \item Soit $n=315$. Calculer, pour tout nombre premier $p$, $v_p(n)$.
    \item Calculer $v_p\left(\frac{12}{25}\right)$ pour $p=2,3,5$.
    \item Vérifier que pour tous $x,y\in\mathbb{Q}$, on a $v_p(x-y)\geq \min(v_p(x),v_p(y))$, avec égalité si $v_p(x)=v_p(y)$.
    \item On définit maintenant la valeur absolue $p$-adique comme l'application $|\cdot|_p:\mathbb{Q}\longrightarrow \mathbb{R}$ vérifiant $|0|_p=0$ et
    \[
      |n|_p=\frac{1}{p^{v_p(n)}},\qquad n\in\mathbb{Q}^*,
    \]
    puis la distance $p$-adique par $d_p(n,m)=|n-m|_p$.
    \begin{enumerate}
      \item Calculer $|250|_5$ et $|13/1750|_5$ et en déduire que $|250|_5<|13/1750|_5$.
      \item Vérifier que $|\cdot|_p$ est une valeur absolue sur $\mathbb{Q}$ et que $d_p$ est une distance ultramétrique sur $\mathbb{Q}$.
    \end{enumerate}
  \end{enumerate}
\end{exo}


\end{document}
