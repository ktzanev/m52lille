\documentclass[a4paper,10pt,reqno]{amsart}
\usepackage{M52}


\begin{document}

\hautdepage{TD6 - Connexité}


% -----------------------------------
\begin{exo}

  Soit $\mathbb{K} = \mathbb{R} \text{ ou } \mathbb{C}$. Les espaces suivants sont-ils connexes? Connexes par arcs?
  \begin{enumerate}
    \item un $\mathbb{K}$-espace vectoriel normé;
    \item une boule dans un $\mathbb{K}$-espace vectoriel normé;
    \item $\ensemble{(x,y)\in\mathbb{R}^2}{x\not=y}$;
    \item la sphère unité $S(0,1)$ dans  un espace vectoriel normé.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $f \colon I \to \mathbb{R}$ une fonction continue et injective sur un intervalle $I \subset \mathbb{R}$. Montrer que $f$ est strictement monotone.
  \begin{indication}
    On pourra considérer l'image de $\ensemble{(x,y) \in I^2}{x<y}$ par l'application $(x,y) \in I^2 \mapsto f(y)-f(x) \in \mathbb{R}$.
  \end{indication}
\end{exo}


% -----------------------------------
\begin{exo}

  Montrer que les espaces suivants ne sont pas homéomorphes:
  \begin{enumerate}
    \item $\mathbb{R}$ et $\mathbb{R} \setminus \{x_0\}$;
    \item  $\mathbb{R}^2\setminus \{x_0\}$, le cercle $C(0,1) \subset \mathbb{R}^2$, et un intervalle de $\mathbb{R}$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $f \colon [a,b] \to \mathbb{C}$ une application continue. Supposons que $f(t)^2=1$ pour tout $t \in [a,b]$. Montrer que $f$ est constante. Que peut-on dire si on suppose $e^{if(t)}=1$ pour tout $t \in [a,b]$?
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $E$ un $\mathbb{K}$-espace vectoriel normé de dimension supérieure ou égale à $2$. Montrer que pour toute application continue $f\colon S(0,1) \to \mathbb{R}$ il existe $w \in S(0,1)$ tel que $f(w)=f(-w)$ (on pourra introduire la fonction $t \mapsto f(t)-f(-t)$).
\end{exo}


% -----------------------------------
\begin{exo}

  On note $F=\{0\}\times[-1;1]\cup[-1;1]\times\{0\}$, muni de la topologie induite par $\mathbb{R}^2$.
  \begin{enumerate}
    \item Montrer que $F$ est compact et connexe.
    \item Montrer que si $f:F\to\mathbb{R}$ est continue, alors $f(F)$ est un segment.
    \item Déterminer les points $x\in F$ pour lesquels $F\setminus\{x\}$ est connexe.
    \item Montrer que $F$ n'est homéomorphe à aucune partie de $\mathbb{R}$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo} \emph{(Partie connexe non connexe par arcs)}

  Soit $\Gamma \subset \mathbb{R}^2$ le graphe de la fonction $x \in ]0,1] \mapsto \sin(1/x)$.
  \begin{enumerate}
    \item Montrer que $\Gamma$ est connexe par arcs.
    \item Montrer que son adhérence $\fermeture{\Gamma}$ (que l'on précisera) est connexe.
    \item On va montrer que $\fermeture{\Gamma}$ n'est pas connexe par arcs. Pour cela on raisonne par l'absurde et on suppose qu'il existe un chemin continu $\gamma=(\gamma_x,\gamma_y)$ tel que $\gamma(0)=(1, \sin(1))$ et $\gamma(1)=(0,0)$.
    \begin{enumerate}
      \item Soit $B=\ensemble{t\in[0,1]}{\gamma_x(t)=0}$. Justifier l'existence de $b:=\inf B$. Montrer que $b>0$ et que $\gamma_x(t)\neq 0$ pour tout $t\in [0,b[$.
      \item Montrer que pour tout $a\in[0,b[$, l'ensemble $\frac1{\gamma_x}(]a,b[)$ contient un segment de longueur $2\pi$. En déduire qu'il existe $s,t\in]a,b[$ tels que $\gamma_y(s)=1$ et $\gamma_y(t)=-1$.
      \item En déduire que $\fermeture\Gamma$ n'est pas connexe par arc.
    \end{enumerate}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.49]

  Montrer que $GL_n(\mathbb{R})$ n'est pas connexe, mais que $GL_n(\mathbb{C})$ est connexe par arcs.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $O$ un ouvert d'un $\mathbb{K}$-espace vectoriel normé $E$.
  \begin{enumerate}
    \item Montrer que, pour tout $x\in O$, $\ensemble{y\in O}{\exists\gamma\in\mathcal{C}([0;1],O),\ \gamma(0)=x,\ \gamma(1)=y}$ est un ouvert de $E$. En déduire une partition de $O$ en ouverts connexes par arcs disjoints.
    \item Montrer que $O$ est connexe si et seulement si $O$ est connexe par arcs.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Si $U$ est une partie ouverte de la droite réelle $\mathbb{R}$, munie de la métrique usuelle, montrer que
  \begin{enumerate}
    \item $U$ est connexe si et seulement si $U$ est un intervalle ouvert;
    \item lorsque $U$ n'est pas connexe, on peut trouver une famille dénombrable ou finie d'intervalles ouverts deux à deux disjoints $I_k$ telle que $U=\cup_kI_k$.
  \end{enumerate}
  Que peut-on dire d'un fermé de $\mathbb{R}$ en rapport avec les intervalles fermés ?
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $\mathcal{P}$ une partie dénombrable de $\mathbb{R}^2$. Montrer que le complémentaire de $\mathcal{P}$ est connexe par arcs.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $K$ un compact dans un espace vectoriel normé de dimension supérieure ou égale à $2$.
  \begin{enumerate}
    \item Montrer que ${K}^c $ admet une unique composante connexe non bornée, notée $C_\infty$.
    \item Soit $a \notin K \cup C_\infty$. Montrer qu'il existe $R$ tel que $S(a,R) \subset C_\infty$, puis que l'application
    \[
      p \colon x \in K \mapsto a + R \frac{x-a}{|x-a|} \in E
    \]
    définit une surjection continue de $K$ sur $S(a,R)$.
    \item En déduire que si $E$ est de dimension infinie, alors le complémentaire d'un compact $K$ est toujours connexe. Ce résultat est-il vrai en dimension finie?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $I$ un intervalle ouvert non vide de $\mathbb{R}$ et $f:I\to \mathbb{R}$ une fonction dérivable. On note $A$ le sous-ensemble de $\mathbb{R}^2$ défini par $A=\ensemble{(x,y)\in I^2}{x<y}$. Soit encore $g:A\to\mathbb{R}$ définie par $g(x,y)=\frac {f(y)-f(x)}{y-x}$.
  \begin{enumerate}
    \item Montrer que $A$ est une partie connexe de $\mathbb{R}^2$.
    \item Montrer que $g(A)$  est une partie connexe de $\mathbb{R}$.
    \item Montrer que $f'(I)\subset \fermeture{g(A)}$.
    \item Montrer que $g(A)\subset f'(I)$.
    \item Montrer que $f'(I)$ est un intervalle.
    \item Si $I$ est ouvert, peut-on en conclure que $f'(I)$ est ouvert ?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(X,d)$ un espace métrique non vide. On dit que $X$ est bien chaîné si pour tout $\varepsilon>0$ et tous $x,y\in X$, il existe $n\in\mathbb{N}$, $x_0=x,x_1,\ldots,x_{n-1}, x_n=y\in X$ tels que
  \[
    \text{pour } j\in\{0,\cdots,n-1\}, \qquad d(x_j,x_{j+1})<\varepsilon.
  \]
  \begin{enumerate}
    \item Pour $x\in X$ et $\varepsilon>0$, on pose $C_\varepsilon(x)=\ensemble{y\in X}{\exists n\in\mathbb{N}, x_0=x, x_1,\ldots, x_{n-1},x_n=y$ tels que pour tout $j, d(x_j,x_{j+1})<\varepsilon}$.  Montrer que $C_\varepsilon(x)$ est ouvert et fermé dans $X$. En déduire que si $X$ est connexe, alors $X$ est bien chaîné.
    \item On suppose dans cette question que $X$ est compact et bien chaîné.
    \begin{enumerate}
      \item Soit $A$ et $B$ deux fermés disjoints de $X$. Montrer qu'il existe $\varepsilon>0$ tel que pour tout $a\in A$ et tout $b\in B$, on ait $d(a,b)\geq \varepsilon$.
      \item Montrer que $X$ est connexe.
    \end{enumerate}
    \item Si $X$ est seulement fermé et bien chaîné, est-il toujours connexe ? Justifier ou donner un contre-exemple.
  \end{enumerate}
\end{exo}

\end{document}

