\documentclass[a4paper,10pt,reqno]{amsart}
\usepackage{M52}

\newcommand*{\app}[5]
{#1: \left\{
\begin {array}{ccl}
  #2 & \longrightarrow & #3 \\
  #4 & \longmapsto & #5
\end {array}\right.}

\begin{document}

\hautdepage{TD3 - Applications continues}


% ==================================
\section{Propriétés générales de la continuité}
% ==================================


% -----------------------------------
\begin{exo}[.7]

  Exhiber deux espaces métrique $(X,d)$ et $(Y,d')$ et une application continue $f:(X,d)\to (Y,d')$ tels que $\mathcal{U}$ ouvert de $X$ n'implique pas que $f({\mathcal{U}})$ soit ouvert dans $Y$.
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soit $f:(X,d)\to (Y,d')$ une application entre 2 espaces métriques. Montrer que $f$ est continue si et seulement si pour toute partie $A$ de $X$, $f(\overline A)$ est inclus dans $\overline{f(A)}$.
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soit $E$ un espace métrique et $f,g:E\to \mathbb{R}$ deux applications continues. Que peut-on dire de $A=\ensemble{x\in E}{f(x)=g(x)}$, $B=\ensemble{x\in E}{f(x)>g(x)}$ et $C=\ensemble{x\in E}{f(x)\geq g(x)}$ ?
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soient $f$ et $g$ deux applications continues d'un espace métrique $E_1$ dans un espace métrique $E_2$.
  \begin{enumerate}
    \item Montrer que $A=\ensemble{x\in E_1}{f(x)=g(x)}$ est fermé dans $E_1$.
    \item En déduire que si $f=g$ sur une partie $B$ dense de $E_1$, alors $f=g$ sur $E_1$.\\
    \emph{En particulier, deux applications définies et continues sur $\mathbb{R}$, qui coïncident sur $\mathbb{Q}$, sont égales.}
  \end{enumerate}
\end{exo}


% ==================================
\section{Exemples d'applications continues}
% ==================================


% -----------------------------------
\begin{exo}

  Soit $(E,\|\cdot\|)$ un espace vectoriel normé sur $\mathbb{R}$. Montrer que
  \begin{enumerate}
    \item Toute application constante sur $E$ est continue.
    \item Pour tout $\lambda\in\mathbb{R}$ et tout $a\in E$, l'application $\app{f}{E}{E}{x}{f(x)=\lambda x+a}$ est lipschitzienne.
    \item Montrer que $x\mapsto \|x\|$ est 1-lipschitzienne, et donc continue.
    \item Vérifier que l'application $f\mapsto\int_0^1 f(t) \dd t$ est continue de $\big(\C{[0,1]},\|\cdot\|_1\big)$ dans $\mathbb{R}$.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}

  Soit $E=\C{[-1,1],\mathbb{R}}$. Les applications
  \[
    \app{A}{E}{\mathbb{R}}{f}{\int_{-1}^1 f(t) \dd t}
    \text{ et }
    \app{B}{E}{\mathbb{R}}{f}{f(0)}
  \]
  sont-elles continues sur $E$ si $E$ est muni de la norme :
  \begin{enumerate}[(a)]
    \item $\|\cdot\|_\infty$, définie par  $\|f\|_\infty=\sup_{[-1,1]} |f(t)|$ ?
    \item $\|\cdot\|_1$, définie par  $\|f\|_1=\int_{-1}^1|f(t)|\dd t$ ?
    \item $\|\cdot\|_2$, définie par  $\|f\|_2=\left(\int_{-1}^1|f(t)|^2\dd t\right)^{\frac12}$ ?

  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $E=\mathbb{R}[X]$. Pour $P(X)=\sum_{k=0}^na_kX^k$, on pose $\|P\|=\max\ensemble{|a_k|}{0\le k\le n}$,
  \[
    u(P)(X)=\sum_{k=1}^n\frac{1}{k}a_kX^k \text{ et } v(P)(X)=\sum_{k=1}^nka_kX^k.
  \]
  Montrer que $\|\cdot\|$ définit une norme sur $E$ et que $u$ et $v$ sont des applications linéaires sur $E$. Les applications $u$ et $v$ sont-elles continues sur $(E,\|\cdot \|)$?
\end{exo}


% ==================================
\section{Homéomorphismes}
% ==================================


% -----------------------------------
\begin{exo}

  Soit $E$ un espace métrique, $d$ et $\delta$ deux distances sur $E$ et $\app{id}{(E,d)}{(E,\delta)}{x}{x}$
  \begin{enumerate}
    \item Montrer que $d$ et $\delta$ sont topologiquement équivalentes si et seulement si $id$ est un homéomorphisme.
    \item En déduire que si $f:(X,d)\to(Y,\delta)$ est un homéomorphisme, alors $d$ et $(x,y)\mapsto\delta(f(x),f(y))$ sont topologiquement équivalentes.
    \item Montrer que $d$ et $\delta$ sont équivalentes si et seulement si $f$ et $f^{-1}$ sont lipschitziennes.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $E$ un espace métrique, $d_1$ et $d_2$ deux métriques sur $E$. Pour $x=(x_1,x_2)$ et $y=(y_1,y_2)\in E\times E$, on pose $\delta_1(x,y)=\max(d_1(x_1,y_1), d_1(x_2,y_2))$ et $\delta_2(x,y)=\max(d_2(x_1,y_1), d_2(x_2,y_2))$.\\
  Montrer que $d_1$ et $d_2$ sont topologiquement équivalentes si et seulement si $d_1:(E\times E, \delta_2)\to(\mathbb{R},|\cdot|)$ et $d_2:(E\times E, \delta_1)\to(\mathbb{R},|\cdot|)$ sont continues.\\
  En particulier, $d_1$ est continue par rapport à $\delta_1$.
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soit $f:(E,d_E)\to (F,d_F)$ une isométrie entre deux espaces métriques, c.-à-d. $\forall x,y\in E$ on a $d_F(f(x),f(y))=d_E(x,y)$. Montrer qu'alors $f$ est un homéomorphisme sur son image. Est-elle nécessairement surjective?
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Montrer (sans appliquer le théorème de Heine!) que l'application $x\mapsto\sqrt{x}$ est uniformément continue sur $[0;1]$ (on pourra raisonner par l'absurde et construire deux suites $(x_n)$ et $(y_n)$).
\end{exo}

% -----------------------------------
\begin{exo}[.7]

  Montrer que l'application $x\mapsto\sqrt[3]{x}$ est uniformément continue sur $\mathbb{R}$. Est-ce que sa réciproque $x\mapsto x^{3}$ est uniformément continue sur $\mathbb{R}$ ?
\end{exo}

% ==================================
\section{Espaces d'applications et continuité}
% ==================================


% -----------------------------------
\begin{exo}

  On munit ${\mathcal{M}}_n(\mathbb{R})$, l'ensemble des matrices carrées de taille $n$, d'une norme $\|\cdot\|$.
  \begin{enumerate}
    \item Montrer que la trace est une application continue de ${\mathcal{M}}_n(\mathbb{R})$ dans $\mathbb{R}$.
    \item Montrer que le déterminant est une application continue de ${\mathcal{M}}_n(\mathbb{R})$ dans $\mathbb{R}$. En déduire que $GL_n(\mathbb{R})$, l'ensemble des matrices de taille $n$ inversibles, est ouvert.
    \item Montrer que $A_p=\ensemble{M\in {\mathcal{M}}_n(\mathbb{R})}{\text{rang}(M)\leq p}$ est fermé. \indic{(On pourra considérer les mineurs de taille $q>p$.)}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Le but est de montrer que l'ensemble $D_n(\mathbb{C})$ des matrices diagonalisables dans $\mathbb{C}$ est dense dans $\mathcal{M}_n(\mathbb{C})$ muni de $N_\infty$ (par exemple).
  \begin{enumerate}[a)]
    \item Soit
    \[
      T=\left(\begin{array}{ccc}\lambda_1&\ &\ast\\ \ &\ddots&\ \\ (0)&\ &\lambda_n\end{array}\right)
    \]
    et pour $k\in\mathbb{N}^*$
    \[
      T_k=\left(\begin{array}{ccc}\lambda_1+\frac{1}{k}&\ &\ast\\ \ &\ddots&\ \\ (0)&\ &\lambda_n+\frac{n}{k}\end{array}\right).
    \]
    Montrer que $T_k\to T$ et que les matrices $T_k$ sont diagonalisables pour $k$ assez grand \indic{(rappelons qu'une condition \emph{suffisante} pour être dans $D_n(\mathbb{C})$ est d'avoir $n$ valeurs propres distinctes)}.
    \item Soit $A\in\mathcal{M}_n(\mathbb{C})$: expliquer pourquoi il existe $P\in GL_n(\mathbb{C})$ et $T$ triangulaire supérieure telles que $A=PTP^{-1}$. Montrer que l'application linéaire $M\mapsto PMP^{-1}$ est continue sur $\mathcal{M}_n(\mathbb{C})$. En déduire que $PT_kP^{-1}\to A$.
    \item Conclure.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(X,d_X)$ et $(Y,d_Y)$ deux espaces métriques. On note $F_b(X,Y)$ (respectivement $C_b(X,Y)$) l'espace des applications bornées (respectivement bornées et continues) de $X$ dans $Y$. On définit pour $f,g\in F_b(X,Y)$
  \[
    \delta(f,g)=\sup_{x\in X}d_Y(f(x),g(x)).
  \]
  \begin{enumerate}[a)]
    \item Vérifier que $\delta$ est une distance sur $F_b(X,Y)$.
    \item Montrer que $C_b(X,Y)$ est fermé dans $F_b(X,Y)$.
  \end{enumerate}
\end{exo}


% ==================================
\section{Calculs de normes\\d'applications linéaires continues}
% ==================================


% -----------------------------------
\begin{exo}

  Soit $E=\ell^\infty(\mathbb{R})$ muni de la norme $\|\cdot\|_\infty$. Pour $u=(u_n)_{n\geq 0}\in\ell^\infty(\mathbb{R})$, on pose $T:E\longrightarrow E$ et $\Delta:E\longrightarrow E$ définis par
  \[
    T(u)=(u_{n+1})_{n\geq 0}\quad \mbox{et}\quad \Delta(u)=(u_{n+1}-u_n)_{n\geq 0}.
  \]
  \begin{enumerate}[(a)]
    \item Montrer que $T$ et $\Delta$ sont des applications linéaires continues de $E$ dans $E$.
    \item Calculer leur norme.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $E=\C{[0,1];\mathbb{R}}$ et $F=\C[1]{[0,1];\mathbb{R}}$ munis des normes usuelles :
  \begin{align*}
    N_1(f) &        =\|f\|_\infty        &\text{pour}\quad & f\in E,\quad \\
    N_2(g) & =\|g\|_\infty+\|g'\|_\infty &\text{pour}\quad & g\in F.\quad
  \end{align*}
  On pose
    \[
      T(f)(x)=\int_0^x f(t)\dd t\qquad f\in E, x\in [0,1].
    \]
  \begin{enumerate}
    \item Vérifier que si $f\in E$, alors $T(f)\in F$.
    \item Montrer que $T$ définit une application linéaire et continue de $(E,N_1)$ dans $(F,N_2)$.
    \item Calculer la norme de $T$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  On munit l'espace $E=\C{[0,1];\mathbb{R}}$ de la norme $\|\cdot\|_2$. Rappelons que
  \[
    \|f\|_2^2=\int_0^1 |f(t)|^2\dd t\qquad f\in E.
  \]
  Pour $f\in E$ et $\varphi\in E$, on pose
  \[
    T_\varphi(f)=\int_0^1 f(t)\varphi(t)\dd t.
  \]
  \begin{enumerate}[(a)]
    \item Montrer que $T_\varphi$ est une application linéaire continue de $E$ dans $\mathbb{R}$ et calculer sa norme.
    \item Même question en remplaçant la norme $\|\cdot\|_2$ par la norme $\|\cdot\|_\infty$.\\
    \begin{indication}
      Pour le calcul de la norme au (b), on pourra introduire la fonction $f_\varepsilon:t\longmapsto \displaystyle\frac{\varphi(t)}{|\varphi(t)|+\varepsilon}$, $\varepsilon>0$.
    \end{indication}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $E=\C{[-1,1];\mathbb{R}}$ muni de la norme $\|\cdot\|_\infty$. On définit pour $f\in E$,
  \[
    T(f)=\int_0^1 f(t)\dd t-\int_{-1}^0 f(t)\dd t.
  \]
  \begin{enumerate}
  \item Montrer que $T$ est une forme linéaire continue sur $E$ et que $\|T\|=2$.\\
  \begin{indication}
    Pour le calcul de la norme, considérer la suite $(f_n)_n$ définie par
    \[
      f_n(x) =
      \begin{cases}
        -1 & \mbox{si }-1\leq x\leq -\frac{1}{n}\\
        nx & \mbox{si }-\frac{1}{n} \leq x\leq \frac{1}{n}\\
        1 & \mbox{si } \frac{1}{n}\leq x\leq 1 .
      \end{cases}
    \]
  \end{indication}
  \item Existe-t-il $f\in E$ de norme $1$ telle que $|T(f)|=2$?
  \end{enumerate}
\end{exo}

\end{document}

