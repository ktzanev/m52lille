\documentclass[a4paper,10pt,reqno]{amsart}
\usepackage{M52}


\begin{document}

\hautdepage{TD2 - Notions de base dans les espaces topologiques}

% ==================================
\section{Ouverts et fermés}
% ==================================


% -----------------------------------
\begin{exo}

  \begin{enumerate}
    \item On munit $\mathbb{R}$ de la distance usuelle $d(x,y)=|x-y|$. L'intervalle $[0,1[$ est-il ouvert ? Fermé ?
    \item On munit $\mathbb{R}^2$ de la métrique euclidienne : $d_2(a,b)= \sqrt{(x_a-x_b)^2+(y_a-y_b)^2}$ où $a=(x_a,y_a)$ et $b=(x_b,y_b)$. Les ensembles suivants sont-ils ouverts ? Fermés ?
    \begin{itemize}[label=~]
      \item $A=\ensemble{(x,y)}{-1<x<1, -1<y<1}$,
      \item $B=\ensemble{(x,y)}{-1<x<1, -1\leq y<1}$,
      \item $C=\ ]0,1[\ \times \{0\}$,
      \item $D=[0,1]\times \{0\}$.
    \end{itemize}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Parmi les sous-ensembles suivants, préciser ceux qui sont ouverts, fermés (toujours en justifiant soigneusement)
  \begin{enumerate}
    \item Les sous-ensembles de $\mathbb{R}$ :\\
      \begin{itemize*}[label=\quad]
        \item $\mathbb{R}$,
        \item $\mathbb{Q}$,
        \item $[0,1]$,
        \item $[0,1[$,
        \item $]-\infty,1]$,
        \item $[1,+\infty[$,
        \item $\ensemble{\frac1n}{n\in\mathbb{N}^*}$,
        \item $\ensemble{\frac1n}{n\in\mathbb{N}^*}\cup \{0\}$.
      \end{itemize*}
    \item Les sous-ensembles de $\mathbb{R}^{2}$ :\\
      \begin{itemize*}[label=]
        \item $]-2,1[\times[0,3]$,
        \item $[0,1]\times \{9\}$,
        \item $\ensemble{(x,y)\in\mathbb{R}^2}{y=x^2}$,\\
        \item $\ensemble{(x,y)\in\mathbb{R}^2}{y-x^3>0}$,
        \item $\ensemble{(x,y)\in\mathbb{R}^2}{yx<1}$,\\
        \item $\ensemble{(x,y)\in\mathbb{R}^2}{a<x<b}$,
        \item $\ensemble{(x,y)\in\mathbb{R}^2}{|x|+|y|<1}$,\\
        \item $\ensemble{(x,y)\in\mathbb{R}^2}{x^2-y^2\geq 1,\; x^2+y^2<2}$,\\
        \item $\ensemble{(x,y)}{x^2-y^2 \geq 3,\; x^2+y^2<1}$.
      \end{itemize*}
  \end{enumerate}

\end{exo}


% -----------------------------------
\begin{exo}

  Soit $E=\C{[0;1],\mathbb{R}}$ muni de la norme $\|\cdot\|_\infty$ définie par $\|f\|_\infty=\sup_{[0,1]}|f|$, et soit $A=\ensemble{f\in E}{\forall t\in[0;1],\ f(t)>0}$.\\
  Montrer que pour tout $f\in A$, il existe $r>0$ tel que $B(f,r)\subset A$. Qu'est-ce que cela signifie pour $A$ ?
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soient $(E,d)$ un espace métrique, $a\in E$ et $F$ un fermé de $E$ tel que $a$ n'appartienne pas à $F$. Montrer qu'il existe deux ouverts disjoints $U$ et $V$ tels que $a$ appartienne à $U$ et $F$ soit inclus dans $V$.
\end{exo}


% -----------------------------------
\begin{exo}[.35]

  Soit $(E,d)$ un espace métrique. Montrer que les singletons sont fermés.
\end{exo}


% -----------------------------------
\begin{exo}

  Soient $(E,d)$ un espace métrique, $A$, $B$, $A_1,\ldots A_n$  des parties de $E$. Montrer que
  \begin{enumerate}
    \item $\fermeture{\fermeture{A}} = \fermeture{A}$, $\interieur{A} = \interieur{\interieur{A}}$,
    \item $\fermeture{\interieur{A}} \subset \fermeture{A}$, $\interieur{A} \subset \interieur{\fermeture{A}}$,
    \item $\interieur{A}\cup \interieur{B}\subset Int(A\cup B)$,
    \item $\fermeture{A\cap B}\subset \fermeture{A}\cap\fermeture{B}$,
    \item $Int\left(\bigcap_{i=1}^n A_i\right)=\bigcap_{i=1}^n \interieur{A_i},$
    \item $\fermeture{\bigcup_{i=1}^n A_i}=\bigcup_{i=1}^n \fermeture{A_i}$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  On munit $\mathbb{R}^n$, $n\geq 1$, de sa topologie usuelle.\\
  {\bf 1.} Donner un exemple d'une partie $A\subset \mathbb{R}$ pour laquelle les 7 ensembles suivants sont distincts :
  \[
    A,\interieur{A},\fermeture{A},\fermeture{\interieur{A}}, \interieur{\fermeture{A}}, \interieur{\fermeture{\interieur{A}}}, \fermeture{\interieur{\fermeture{A}}}.
  \]
  Généraliser à $\mathbb{R}^n$, $n\geq 1$.\\
  {\bf 2.} Montrer que pour toute partie $A$ de $\mathbb{R}^n$, $\fermeture{\interieur{\fermeture{\interieur{A}}}}=\fermeture{\interieur{A}}$ et $\interieur{\fermeture{A}}=\interieur{\fermeture{\interieur{\fermeture{A}}}}$.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(E,\|\cdot\|)$ un espace vectoriel normé.
  \begin{enumerate}
    \item Montrer que pour toute partie $A$ de $E$, on a
    $${\rm Vect}(A)\subset {\rm Vect}(\fermeture{A})\subset \fermeture{{\rm Vect }(A)}.$$
    \item En déduire que pour tout sous-espace vectoriel $F$ de $E$, $\fermeture{F}$ est aussi un sous-espace vectoriel de $E$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(E,\|\cdot\|)$ un espace vectoriel normé. On dit qu'une partie $A$ de $E$ est étoilée s'il existe $a\in A$ tel que pour tout $x\in A$ et tout $\lambda\in [0,1]$, le point $\lambda a+(1-\lambda) x$ appartient encore à $A$. Montrer que si $A$ est étoilé alors $\fermeture{A}$ est encore étoilé.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $E$ un espace vectoriel normé et $A$ et $B$ deux parties de $E$. On définit
  \[
    A+B=\{z\in E:\exists x\in A,\,\exists y\in B,\,z=x+y\}.
  \]
  \begin{enumerate}
    \item Démontrer que si $A$ est ouvert alors $A+B$ est ouvert.
    \item Démontrer que les parties $A=\{(x,y)\in\mathbb R^2:xy=1\}$ et $B=\{0\}\times\mathbb R$ sont fermées mais que $A+B$ n'est pas fermée.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soit $E$ un espace vectoriel normé et $F$ un sous-espace vectoriel de $E$. On suppose que $F$ est ouvert. Démontrer que $F=E$.
\end{exo}


% -----------------------------------
\begin{exo}[.49]

  Montrer que tout ouvert de $\mathbb R^n$ peut s'écrire comme une réunion dénombrable de boules ouvertes.
\end{exo}

% ==================================
\section{Interieur, adhérence, densité, points isolés}
% ==================================


% -----------------------------------
\begin{exo}

  Soit $(E,\|\cdot\|)$ un $\mathbb{K}$-espace vectoriel normé.
  \begin{enumerate}
    \item Montrer que $A\subset E$ est d'intérieur non vide si et seulement si $A$ contient une boule ouverte.\\
    \item En déduire que tout sous-espace vectoriel strict $F$ de $E$ est d'intérieur vide \indic{(raisonner par l'absurde, et montrer que $F$ contient alors $E$)}.
    \item Montrer que si $V$ est un sous-espace vectoriel de $E$, alors $\overline V$ est un sous-espace vectoriel de $E$.
    \item Soit $H$ un hyperplan de $E$. Démontrer que $H$ est ou bien fermé ou bien dense dans $E$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(X,d)$ un espace métrique et $D$ un sous-ensemble de $X$. Montrer que les assertions suivantes sont équivalentes.
  \begin{enumerate}
    \item $D$ est dense dans $X$.
    \item Le complémentaire de $D$ est d'intérieur vide.
    \item Si $F$ est un fermé contenant $D$, alors $F=X$.
    \item  Si $U$ est un ouvert non vide de $X$ alors $U\cap D$ est non vide.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soit $(X,d)$ un espace métrique. Montrer que $A\subset X$ rencontre toute partie dense dans $X$ si et seulement si $\interieur{A} \neq \emptyset$.
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soit $(X,d)$ un espace métrique. Soient $E$ et $G$ deux ouverts denses dans $X$ ; montrer que $E\cap G$ est encore un ouvert dense dans $X$.
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $f$ une application de ${\mathbb{R}}$ dans ${\mathbb{R}}$ telle que pour tout $a>0$, l'ensemble des $x$ vérifiant $\vert f(x)\vert>a$ est fini.\\
  Montrer que $X=\ensemble{x}{f(x)=0}$ est dense dans $\mathbb{R}$.
  \begin{indication}
    Montrer que $X^{c} = \ensemble{x}{f(x)\neq 0}$ est dénombrable, puis conclure.
  \end{indication}
\end{exo}


% -----------------------------------
\begin{exo}

  On note $X=\ell^{\infty}(\mathbb{R})$ l'espace des suites réelles bornées muni de la norme $\|\cdot\|_\infty$. Soient $Y$ l'espace des suites réelles tendant vers $0$ et $Z$ l'espace des suites réelles nulles à partir d'un certain rang.
  \begin{enumerate}
    \item Vérifier que $Z\subset Y\subset X$.
    \item Montrer que $Z$ est dense dans $Y$.
    \item Montrer que $Y$ et $Z$ ne sont pas denses dans $X$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  \begin{enumerate}
    \item Soit $(G,+)$ un sous-groupe fermé de $(\mathbb{R},+)$. Montrer que soit $G=\mathbb{R}$, soit $G=\{0\}$, soit $G=a\mathbb{Z}$ où $a=\inf(G\cap \mathbb{R}^{+*})$ (on justifiera que $a$ est bien défini et on montrera que $a$ appartient à $\fermeture{G}$).
    \item Soit maintenant $(G,+)$ un sous-groupe de $(\mathbb{R},+)$ non nécessairement fermé.
    \begin{enumerate}
      \item Montrer que $\fermeture{G}$ est un sous-groupe de $\mathbb{R}$.
      \item En déduire que soit $G$ est dense dans $\mathbb{R}$, soit il existe $a\in\mathbb{R}$ tel que $G=a\mathbb{Z}$.
    \end{enumerate}
    \item Soient $a,b\in\mathbb{R}$ et $H=a\mathbb{Z}+b\mathbb{Z}=\ensemble{ak+bl}{k,l\in\mathbb{Z}}$.
    \begin{enumerate}
      \item Vérifier que $H$ est un sous-groupe de $\mathbb{R}$.
      \item A quelle condition existe-t-il $c\in\mathbb{R}$ tel que $H=c\mathbb{Z}$ ?
      \item En déduire que $H$ est dense dans $\mathbb{R}$ si et seulement si $\frac ab\notin \mathbb{Q}$.
    \end{enumerate}
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}[.49]

  On munit $\mathbb{R}$ de la distance usuelle. Déterminer les points isolés et les points d'accumulation de $\mathbb{Q}$.
\end{exo}


% -----------------------------------
\begin{exo}[.7]

  Soient $(E,d)$ un espace métrique, $A$ un sous-ensemble de $E$. Montrer que l'ensemble des points d'accumulation de $A$ est toujours fermé. Qu'en est-il de ses points isolés?
\end{exo}


% -----------------------------------
\begin{exo}

  Soient $(E,d)$ un espace métrique, $A$ un sous-ensemble de $E$.
  \begin{enumerate}
    \item Montrer que $\partial A=\partial (E\setminus A)$.
    \item Soit $x\in \fermeture{A}\setminus A$. Montrer que pour tout ouvert $\mathcal{V}$ contenant $x$, ${\mathcal{V}}\cap A$ n'est jamais fini.
    \item En déduire que, si $A$ est ouvert, tout $x\in \partial A$ est un point d'accumulation de $A$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Déterminer les valeurs d'adhérence de chacune des suites $(u_k)_{k\ge 0}$ suivantes :
  \vspace{-7pt}
  \begin{multicols}{2}
    \begin{enumerate}
      \item $u_k=(-1)^k$,
      \item $u_k=\sin\frac{k\pi}3$,
      \item $u_k=e^{\sqrt 2 ki}$,
      \item $u_k=(-1)^k\,k$.
    \end{enumerate}
  \end{multicols}
\end{exo}


% ==================================
\section{Diamètre}
% ==================================


% -----------------------------------
\begin{exo}

  On munit $\C{[0,1]}$ de la métrique $d$ définie par $d(f,g)=\int_0^1 \left|f(x)-g(x)\right| dx$. Déterminer le diamètre de
  \begin{enumerate}
    \item $\ensemble{f\in \C{[0,1]}}{0\leq f\leq 1}$;
    \item $\ensemble{f\in \C{[0,1]}}{0\leq f\leq 1,\ f(0)=0}$.
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Soient $(E,d)$ un espace métrique, et $A$ et $B$ des parties de $E$.
  \begin{enumerate}
    \item Montrer que si $A\subset B$ alors $\diam A\leq \diam B$.
    \item Montrer que  $\diam\mathring{A}\leq \diam A=\diam \fermeture{A}$.
    \item Montrer par un exemple que l'inégalité $\diam\mathring{A}\leq \diam A$ peut être stricte.
    \item Montrer que $\diam(\mbox{Fr}(A))\leq \diam A$.
    \item On suppose dans cette question que $E$ est un espace vectoriel normé et que $A$ est une partie non vide et bornée de $E$.
    \begin{enumerate}
    \item Soit $x\in A$ et $u\in E$, $u\neq 0$. On considère $X=\{t\geq 0: x+tu\in A\}$. Justifier que $\sup X$ existe dans $\mathbb R$.
    \item En déduire que toute demi-droite issue d'un point $x$ de $A$ rencontre $\mbox{Fr}(A)$ (i.e. l'intersection entre la demi-droite et la frontière de $A$ est non vide).
    \item En déduire que $\diam(\mbox{Fr}(A))=\diam A$.

    \end{enumerate}
  \end{enumerate}
\end{exo}


% ==================================
\section{Topologie induite, topologie produit}
% =================================


% -----------------------------------
\begin{exo}
  Soient $(X_i,d_i)_{1\leq i\leq n}$ une famille finie d'espaces métriques, $X=\prod_{i=1}^n X_i$ le produit cartésien  muni de la distance usuelle
  \[
    d_{\infty}(x,y)=\max_{1\leq i\leq n}d_i(x_i,y_i),
  \]
  pour $x=(x_1,\dots,x_n), y=(y_1,\dots,y_n)\in X$. Le but de l'exercice est de démontrer les deux résultats suivants énoncés en cours :
  \begin{enumerate}
    \item Montrer qu'une suite $(x^{(k)})_k=((x_1^{(k)},\dots x_n^{(k)}))_k$ de $X$ converge vers $x=(x_1,\dots,x_n)$ dans $(X,d_\infty)$ si et seulement si pour tout $1\leq i\leq n$, la suite $(x_i^{(k)})_k$ tend vers $x_i$ dans $(X_i,d_i)$.
    \item Soit $x=(x_1,\dots,x_n)\in X$ et $r>0$. Montrer que
    \begin{equation}\label{eq:produit-boules}
      B_X(x,r)=\prod_{i=1}^n B_{X_i}(x_i,r),
    \end{equation}
    où $B_X(x,r)$ (respectivement $B_{X_i}(x_i,r)$) désigne la boule ouverte de centre $x$ et de rayon $r$ dans $X$ (respectivement la boule ouverte de centre $x_i$ et de rayon $r$ dans $X_i$).
    \item Est-ce que la formule \eqref{eq:produit-boules} est vraie si on remplace les boules ouvertes par les boules fermées?
    \item Est-ce que la formule \eqref{eq:produit-boules} est vraie si on remplace la distance $d_\infty$ par une distance équivalente, par exemple par $d_1(x,y)=\sum_{i=1}^n d_i(x_i,y_i)$?
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  On rappelle (voir cours) que si $(X_i,d_i)_{1\leq i\leq n}$ est une famille finie d'espaces métriques et si pour tout $1\leq i\leq n$, $\mathcal O_i$ est un ouvert de $X_i$, alors $\mathcal O=\mathcal O_1\times\dots\times\mathcal O_n$ est un ouvert de $X=\prod_{i=1}^n X_i$ (muni de la distance usuelle $d_\infty$). Que pensez-vous de la réciproque? Autrement dit, est-ce qu'un ouvert de $X$ s'écrit toujours comme le produit cartésien d'ouverts de $X_i$?
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $(X,d)$ un espace métrique et $A\subset X$. On munit $A$ de la topologie induite.
  Le but de l'exercice est de démontrer les résultats suivants vus en cours (mais dont la preuve a été laissée en exercice) :
  \begin{enumerate}
    \item Soit $\mathcal O\subset A$. Montrer que $\mathcal O$ est un ouvert de $A$ si et seulement si il existe un ouvert $\Omega$ de $X$ tel que $\mathcal O=\Omega\cap A$.
    \item Soit $F\subset A$. Montrer que $F$ est un fermé de $A$ si et seulement si il existe un fermé $G$ de $X$ tel que $F=G\cap A$.
    \item Soit $B\subset A$. Montrer que $\mbox{Int}_X(B)\cap A\subset \mbox{Int}_A(B)$, où $\mbox{Int}_X(B)$ (respectivement $\mbox{Int}_A(B)$) désigne l'intérieur de $B$ dans $(X,d)$ (respectivement dans $(A,d_A)$ avec $d_A=d|A\times A$). Donner un exemple montrant qu'il n'y a pas égalité en général.
    \item Soit $B\subset A$. Montrer que $\overline{B}^A=\overline{B}\cap A$, où $\overline{B}$ (respectivement $\overline{B}^A$) désigne l'adhérence de $B$ dans $X$ (respectivement dans $A$).
  \end{enumerate}
\end{exo}


% -----------------------------------
\begin{exo}

  Montrer que l'intervalle $]1,2]$ est un fermé de $]1,3[$ pour la topologie induite par $\mathbb R$. Est-ce que $]1,2]$ est un fermé de $\mathbb R$?
\end{exo}


% -----------------------------------
\begin{exo}

  Soit $A$ un sous-ensemble d'un espace métrique $(X,d)$.
  \begin{enumerate}
    \item Montrer que si $A$ est un ouvert de $X$, alors les ouverts de $A$ sont les ouverts de $X$ contenus dans $A$.
    \item Montrer que si $A$ est un fermé de $X$, alors les fermés de $A$ sont les fermés de $X$ contenus dans $A$.
  \end{enumerate}
\end{exo}


\end{document}

